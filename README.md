# >> Project abandoned <<

# Local-multiplayer 2D
_**Please note that the code is prototype code. There's still bugs that should be fixed and code-improvements that could be done.**_

- Uses the new InputSystem
- Players are able to join and leave
- Players able to select a character and color (if a player is ready, that character and color is not available for the rest of the players)
- If all players are ready, it starts counting down and starting the game
- Simple scene transitions
- Character animations for idle, running, jumping, attacking, crouching
- Variable jump height, jumps higher if you hold down the jump button (SPACE for keyboard, X-button for gamepads)

Video showing the features: https://youtu.be/qRPF_vV8RoY  
Reddit thread (with some info): https://www.reddit.com/r/Unity2D/comments/er1p8b/a_prototype_2d_local_multiplayer_game_ive_been/  


## Versions used
Name | Version
--- | ---
Unity | v2020.3 LTS
InputSystem | v1.2.0
Cinemachine | v2.8.4
Universal RP | v10.7.0


## Author information
This project is extracted from one of my prototype games. There's a lot unused scripts in the project that I'm not sure I can remove, a lot of the code doesn't use the [single responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) (because prototyping ya know).

I only have a keyboard + one controller so I can't test 4 players, only 2.


## Player creation flow (starting at Demo menu-scene)
_NOTE! I recommend you take a look at the input actions located in the /Resources/Inputs/ folder._

0. The GameManager is pretty much the [God object](https://en.wikipedia.org/wiki/God_object) in this project. I'm not sure what the GameManager should know about other stuff, so I most scripts use the GameManager-script to fire events through there (so all players are updated).
1. When starting the game there's no player, GameManager has a PlayerInputManager-component that waits until any kind of input is triggered.
2. When an input is triggered that device becomes the first player (first player controls the menu).
3. Menu -> Local multiplayer.
4. First player should already be at the first slot, other players can join here.
5. Players are able to select character and a color.
6. Once players Ready-up those characters and colors are taken and can't be chosen by anyone else.
7. Starts a 5 seconds countdown.
8. When countdown is finished the game transitions to the Demo gameplay-scene.
9. Demo menu -> Demo gameplay scene.
10. Moves the players to spawn points in the level (transforms placed around the level).
11. Builds player UI that shows the color, avatar icon, health at the top of the screen.
12. Enables the players movement (was disabled during the menu).

Press F1 on the keyboard to enable DevMode. While in DevMode you can change character by scrolling the mousewheel up or down (L1/R1 on gamepads).


## Known issues
- Pause menu messes with the players input enable/disable status. Pausing on keyboard disables the gamepad's player inputs and vice versa.
- Some issues when selecting character and color when joining and leaving.


## Credits
- [2D Platformer Hunter by ta-david-yu](https://github.com/ta-david-yu/2D-Platformer-Hunter)
- [Kenney](https://kenney.nl/)
- [Animated Pixel Adventurer by rvros](https://rvros.itch.io/animated-pixel-hero)
- [Kings and Pigs by pixel-frog](https://pixel-frog.itch.io/kings-and-pigs)
- [Auto Letterbox by Tom Elliot](https://tomsoftwares.wordpress.com/2016/06/15/auto-letterbox/)


## License
[MIT License](LICENSE.md)
