﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// NOTE: This script is used by scenes that start directly in the editor. UI and GameManager are setup so the game can be played.
// TODO: Remove this, not needed in this project.

namespace Saucy {
  [DisallowMultipleComponent]
  public class DevLevel : MonoBehaviour {
    [SerializeField] private GameManager gameManager = null;
    [Tooltip("Enables player controls after a delay.")] [SerializeField] private bool comingFromMenu = false;
    [SerializeField] private float timeBeforeEnablingPlayers = 2f;
    [SerializeField] private GameObject UIPrefab = null;
    [SerializeField] private GameObject gameManagerPrefab = null;
    // [SerializeField] private bool devModeUsesSpawnPoints = true;
    [SerializeField] private List<Transform> spawnPoints = new List<Transform>();

    private GameManagerHandler gameManagerObject;

    private void OnValidate () {
      if (spawnPoints.Count < gameManager.MaxPlayers) {
        Debug.LogError($"Not enough spawn points for all players. Check the \"{gameObject.name}\"-gameobject and add more.");
      }
    }

    private void Awake () {
      CheckIfUIIsInScene();
      CheckIfGameManagerIsInScene();

      SetStartingPositions();

      if (comingFromMenu) {
        StartCoroutine(EnablePlayers());
      }

      gameManager.OnLevelChanged?.Invoke(SceneManager.GetActiveScene().buildIndex);
    }

    private void CheckIfUIIsInScene () {
      GameObject _ui = GameObject.FindGameObjectWithTag("UI");

      if (_ui == null) {
        GameObject _clone = (GameObject) Instantiate(UIPrefab);
        _clone.name = "UI (instantiated)";
      }
    }

    private void CheckIfGameManagerIsInScene () {
      GameObject _gm = GameObject.FindGameObjectWithTag("GameManager");

      if (_gm == null) {
        gameManagerObject = (GameManagerHandler) Instantiate(gameManagerPrefab, transform.position, transform.rotation).GetComponent<GameManagerHandler>();
        gameManagerObject.name = "GameManager (instantiated)";
      } else {
        gameManagerObject = _gm.GetComponent<GameManagerHandler>();
      }

      gameManager.StartingSpawnPoints = spawnPoints;
    }

    private void SetStartingPositions () {
      for (int _index = 0; _index < gameManager.MaxPlayers; _index++) {
        if (!gameManager.Players.ContainsKey(_index)) {
          continue;
        }

        gameManager.Players[_index].transform.position = spawnPoints[_index].position;
      }
    }

    private IEnumerator EnablePlayers () {
      yield return new WaitForSeconds(timeBeforeEnablingPlayers);

      gameManager.EnablePlayers();
    }
  }
}
