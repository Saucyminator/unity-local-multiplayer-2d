using UnityEngine;
using TMPro;

namespace Saucy {
  public class DevPlayerInfo : MonoBehaviour {
    [Header("References")] [SerializeField] private TextMeshProUGUI slotLabel = null;
    [SerializeField] private TextMeshProUGUI nameLabel = null;
    [SerializeField] private TextMeshProUGUI isReadyLabel = null;
    [SerializeField] private TextMeshProUGUI isHostLabel = null;
    [Header("Settings")] [Min(0)] [SerializeField] private int playerIndex = 0;

    private void Awake () {
      slotLabel.text = string.Empty;
      nameLabel.text = string.Empty;
      isReadyLabel.text = string.Empty;
      isHostLabel.text = string.Empty;
    }

    public void SetLabels (Player _player) {
      if (_player.PlayerIndex != playerIndex) {
        return;
      }

      slotLabel.text = string.Format("Slot: {0}", _player.PlayerSlot);
      nameLabel.text = string.Format("Name: {0}", _player.Data.Name);
      isReadyLabel.text = string.Format("IsReady: {0}", _player.IsReady);
      isHostLabel.text = string.Format("IsHost: {0}", _player.IsHost);
    }

    public void ClearLabels (Player _player) {
      if (_player.PlayerIndex != playerIndex) {
        return;
      }

      slotLabel.text = string.Empty;
      nameLabel.text = string.Empty;
      isReadyLabel.text = string.Empty;
      isHostLabel.text = string.Empty;
    }
  }
}
