using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

// TODO: BuildIndex isn't correctly set by the engine. It starts off on -1, not 0 as it should.

// NOTE: This handles the scenes and their transitions.

namespace Saucy {
  public class LevelManager : MonoBehaviour {
    [SerializeField] private Animator transition = null;
    [SerializeField] private float transitionTime = 1f;
    [SerializeField] private int mainMenuBuildIndex = 0;
    [Tooltip("Turn off on scenes you want to test stuff on.")] [SerializeField] private bool useTransition = true;
    [Header("Listening on")] [SerializeField] private GameEvent eventLoadMainMenu = null;
    [SerializeField] private GameEvent eventLoadNextLevel = null;

    private int nextBuildIndex;

    private void OnValidate () {
      transition.enabled = useTransition;
    }

    private void Awake () {
      transition.enabled = useTransition;
    }

    private void OnEnable () {
      eventLoadMainMenu.OnEventRaised += LoadMainMenu;
      eventLoadNextLevel.OnEventRaised += LoadNextLevel;
    }

    private void OnDisable () {
      eventLoadMainMenu.OnEventRaised -= LoadMainMenu;
      eventLoadNextLevel.OnEventRaised -= LoadNextLevel;
    }

    public void LoadMainMenu () {
      Debug.Log($"loading main menu...");

      StartCoroutine(LoadLevel(mainMenuBuildIndex));
    }

    public void LoadNextLevel () {
      Debug.Log($"loading next level...");

      if (SceneManager.GetActiveScene().buildIndex < 0) {
        nextBuildIndex = 1;
      } else {
        nextBuildIndex = SceneManager.GetActiveScene().buildIndex + 1;
      }

      StartCoroutine(LoadLevel(nextBuildIndex));
    }

    private IEnumerator LoadLevel (int _levelIndex) {
      Debug.Log($"loading level: {_levelIndex}");

      transition.SetTrigger("Start");

      yield return new WaitForSeconds(transitionTime);

      SceneManager.LoadScene(_levelIndex);
    }
  }
}
