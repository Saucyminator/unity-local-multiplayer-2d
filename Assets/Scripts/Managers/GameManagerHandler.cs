﻿using UnityEngine;
using UnityEngine.InputSystem;

// NOTE: Helper for GameManager

namespace Saucy {
  public class GameManagerHandler : MonoBehaviour {
    [Header("References")] [SerializeField] private GameManager gameManager = null;

    private void Awake () => gameManager.Initialization();
    private void OnEnable () => gameManager.SubscribeToEvents();
    private void OnDisable () => gameManager.UnsubscribeFromEvents();
  }
}
