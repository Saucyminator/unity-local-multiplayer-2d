﻿using UnityEngine;
using Cinemachine;

namespace Saucy {
  public class SetupCamera : MonoBehaviour {
    [SerializeField] private GameManager gameManager = null;
    [SerializeField] private Camera cam = null;

    private void Awake () {
      gameManager.MainCamera = cam;
    }
  }
}
