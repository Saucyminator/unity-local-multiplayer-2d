using UnityEngine;
using UnityEngine.InputSystem;

// NOTE: Updates color and character.

namespace Saucy {
  [DisallowMultipleComponent]
  public class Graphics : MonoBehaviour {
    [SerializeField] private Player player = null;
    [SerializeField] private SpriteRenderer render = null;
    [Header("Listening on")] [SerializeField] private PlayerGameEvent eventEnableInput = null;
    [SerializeField] private PlayerGameEvent eventDisableInput = null;
    [SerializeField] private PlayerGameEvent eventPlayerReady = null;
    [SerializeField] private PlayerGameEvent eventPlayerUnready = null;
    [SerializeField] private PlayerGameEvent eventCharacterAndColorSelected = null;
    [Header("Broadcasting on")] [SerializeField] private PlayerGameEvent onEventPlayerDataUpdated = null;

    private DataColor currentColor;
    private DataCharacter currentCharacter;
    private bool isFacingRight = true;
    private bool inputEnabled = true;

    private void Awake () {
      currentColor = player.Color;
      currentCharacter = player.Data;
    }

    private void OnEnable () {
      eventEnableInput.OnEventRaised += EnableInput;
      eventDisableInput.OnEventRaised += DisableInput;
      eventPlayerReady.OnEventRaised += PlayerReady;
      eventPlayerUnready.OnEventRaised += PlayerUnready;
      eventCharacterAndColorSelected.OnEventRaised += AnotherPlayerHasSelectedCharacterAndColor;

      inputEnabled = true;
    }

    private void OnDisable () {
      eventEnableInput.OnEventRaised -= EnableInput;
      eventDisableInput.OnEventRaised -= DisableInput;
      eventPlayerReady.OnEventRaised -= PlayerReady;
      eventPlayerUnready.OnEventRaised -= PlayerUnready;
      eventCharacterAndColorSelected.OnEventRaised -= AnotherPlayerHasSelectedCharacterAndColor;
    }

    public void OnInputChangeColor (InputAction.CallbackContext _context) {
      if (!inputEnabled) {
        return;
      }

      if (_context.phase == InputActionPhase.Performed) {
        SelectColor((int) _context.ReadValue<Vector2>().y);
      }
    }

    public void OnInputChangeCharacter (InputAction.CallbackContext _context) {
      if (!inputEnabled) {
        return;
      }

      if (_context.phase == InputActionPhase.Performed) {
        SelectCharacter((int) _context.ReadValue<Vector2>().x);
      }
    }

    private void EnableInput (Player _player) {
      if (_player != player) {
        return;
      }

      inputEnabled = true;
    }

    private void DisableInput (Player _player) {
      if (_player != player) {
        return;
      }

      inputEnabled = false;
    }

    private void PlayerReady (Player _player) {
      if (_player != player) {
        return;
      }

      player.GameManager.SelectCharacterAndColor(player, currentCharacter, currentColor);
    }

    private void PlayerUnready (Player _player) {
      if (_player != player) {
        return;
      }

      player.GameManager.UnselectCharacterAndColor(player, currentCharacter, currentColor);
    }

    public void SelectColor (int _selection) {
      int _currentIndex = player.GameManager.SelectableColors.IndexOf(player.Color);
      int _count = player.GameManager.SelectableColors.Count;

      switch (_selection) {
        case -1: // Backwards
          --_currentIndex;

          if (_currentIndex < 0) {
            _currentIndex = _count - 1;
          }
          break;
        case 1: // Forwards
        default:
          ++_currentIndex;

          if (_currentIndex > _count - 1) {
            _currentIndex = 0;
          }
          break;
      }

      currentColor = player.GameManager.SelectableColors[_currentIndex];

      UpdateGraphics();
    }

    public void SelectCharacter (int _selection) {
      int _currentIndex = player.GameManager.SelectableCharacters.IndexOf(player.Data);
      int _availableCount = player.GameManager.SelectableCharacters.Count;

      switch (_selection) {
        case -1: // Backwards
          --_currentIndex;

          if (_currentIndex < 0) {
            _currentIndex = _availableCount - 1;
          }
          break;
        case 1: // Forwards
        default:
          ++_currentIndex;

          if (_currentIndex > _availableCount - 1) {
            _currentIndex = 0;
          }
          break;
      }

      currentCharacter = player.GameManager.SelectableCharacters[_currentIndex];

      UpdateGraphics();
    }

    private void AnotherPlayerHasSelectedCharacterAndColor (Player _player) {
      if (_player != player) {
        if (_player.Data == currentCharacter) {
          currentCharacter = player.GameManager.SelectNextAvailableCharacter(currentCharacter);
        }
        if (_player.Color == currentColor) {
          currentColor = player.GameManager.SelectNextAvailableColor(currentColor);
        }

        UpdateGraphics();
      }
    }

    public void UpdateGraphics () {
      player.Setup(currentCharacter, currentColor);

      render.color = player.Color.Color;
      render.sortingOrder = render.sortingOrder + (player.GameManager.MaxPlayers - player.PlayerSlot);

      onEventPlayerDataUpdated?.Raise(player);
    }

    public void FlipX () {
      isFacingRight = !isFacingRight;
      render.flipX = !isFacingRight;
    }

    public void Show () => render.enabled = true;

    public void Hide () => render.enabled = false;
  }
}
