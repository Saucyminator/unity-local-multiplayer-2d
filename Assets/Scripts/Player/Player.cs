﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using DYP;

// NOTE: Holds references.

// TODO: Fix attack1 animation - when running it looks weird.
// TODO: Remove the fall-through-platform inputs on the new controller script. Messes with the gamepad movement left/right, can't sometimes jump
// TODO: Fix if dying in the air to land on the ground (straight down), reset velocity too.

namespace Saucy {
  [DisallowMultipleComponent]
  public class Player : MonoBehaviour {
    [HideInInspector] public int PlayerIndex = -1; // Remains -1 if it is not set anywhere else.
    [HideInInspector] public int PlayerSlot => PlayerIndex + 1;
    [HideInInspector] public bool IsReady = false;
    [HideInInspector] public bool IsHost = false;

    [Header("References")] public PlayerInput PlayerInput;
    public PlayerInputs PlayerInputs; // TODO: Change
    public PlayerMovementController2D Controller;
    [SerializeField] private Rigidbody2D rb = null;
    [Header("Data")] public GameManager GameManager = null;
    public DataCharacter Data = null;
    public DataColor Color;

    [Header("Broadcasting on")] [SerializeField] private PlayerGameEvent onEventPlayerReady = null;
    [SerializeField] private PlayerGameEvent onEventPlayerUnready = null;

    private float originalGravityScale;

    public PlayerEvent OnSetup;
    public BoolEvent OnInputsEnabled;

    private void Awake () => originalGravityScale = rb.gravityScale;

    private void OnEnable () => Disable();

    public void Setup (DataCharacter _data, DataColor _color) {
      Data = _data;
      Color = _color;

      OnSetup?.Invoke(this);
    }

    public void Enable () {
      rb.gravityScale = originalGravityScale;

      OnInputsEnabled?.Invoke(true);
    }

    public void Disable () {
      rb.gravityScale = 0f;

      OnInputsEnabled?.Invoke(false);
    }

    public void RemovePlayer () {
      StopAllCoroutines();
      Destroy(gameObject);
    }

    public void SetColor (DataColor _newColor) => Color = _newColor;

    public void SetCharacter (DataCharacter _newCharacter) => Data = _newCharacter;

    public void ToggleReadyStatus () {
      IsReady = !IsReady;

      if (IsReady) {
        onEventPlayerReady?.Raise(this);
      } else {
        onEventPlayerUnready?.Raise(this);
      }
    }
  }
}
