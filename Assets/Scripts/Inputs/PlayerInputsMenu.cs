using UnityEngine;
using UnityEngine.InputSystem;

namespace Saucy {
  [RequireComponent(typeof(Player))]
  public class PlayerInputsMenu : MonoBehaviour {
    [SerializeField] private Player player = null;

    // NOTE: Currently not used because PlayerInputManager is handling the inputs directly. Keeping this input so we know which inputs are being used for joining.
    public void OnInputJoinGame (InputAction.CallbackContext _context) { }

    public void OnInputLeaveGame (InputAction.CallbackContext _context) {
      if (_context.phase == InputActionPhase.Performed && !player.IsHost) {
        player.GameManager.RemovePlayer(player);
      }
    }

    public void OnInputReady (InputAction.CallbackContext _context) {
      if (_context.phase == InputActionPhase.Performed) {
        player.ToggleReadyStatus();
      }
    }

    public void OnInputPause (InputAction.CallbackContext _context) {
      if (_context.phase == InputActionPhase.Performed) {
        // player.PlayerState.OnPaused();
      }
    }

    public void OnInputUnpause (InputAction.CallbackContext _context) {
      if (_context.phase == InputActionPhase.Performed) {
        // player.PlayerState.OnUnpaused();
      }
    }

    public void OnInputBack (InputAction.CallbackContext _context) {
      if (_context.phase == InputActionPhase.Performed) {
        // player.PlayerState.OnBack();
      }
    }
  }
}
