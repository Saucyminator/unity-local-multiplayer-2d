using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

namespace Saucy {
  [DisallowMultipleComponent]
  [RequireComponent(typeof(PlayerInput))]
  public class InputManager : MonoBehaviour {
    [SerializeField] private PlayerInput playerInput;
    [SerializeField] private string actionMapPlayer = "Player";
    [SerializeField] private string actionMapUI = "UI";
    [SerializeField] private string actionMapUISelectionMenu = "UI Selection Menu";

    private string currentControlScheme;
    private string currentActionMap => (playerInput.currentActionMap != null) ? playerInput.currentActionMap.ToString() : "";

    public void EnableControlsPlayer () {
      playerInput.SwitchCurrentActionMap(actionMapPlayer);
    }

    public void EnableControlsUI () {
      playerInput.SwitchCurrentActionMap(actionMapUI);
    }

    public void EnableControlsUISelectionMenu () {
      playerInput.SwitchCurrentActionMap(actionMapUISelectionMenu);
    }

    public void SetInputActiveState (bool _gameIsPaused) {
      switch (_gameIsPaused) {
        case true:
          playerInput.DeactivateInput();
          break;
        case false:
          playerInput.ActivateInput();
          break;
      }
    }

    public void OnControlsChanged (PlayerInput _playerInput) {
      if (playerInput.currentControlScheme != currentControlScheme) {
        currentControlScheme = playerInput.currentControlScheme;

        RemoveAllBindingOverrides();
      }
    }

    private void RemoveAllBindingOverrides () {
      InputActionRebindingExtensions.RemoveAllBindingOverrides(playerInput.currentActionMap);
    }

    public void OnDeviceLost (PlayerInput _playerInput) {
      // playerVisualsBehaviour.SetDisconnectedDeviceVisuals();
    }

    public void OnDeviceRegained (PlayerInput _playerInput) {
      StartCoroutine(WaitForDeviceToBeRegained());
    }

    private IEnumerator WaitForDeviceToBeRegained () {
      yield return new WaitForSeconds(0.1f);
      // playerVisualsBehaviour.UpdatePlayerVisuals();
    }
  }
}
