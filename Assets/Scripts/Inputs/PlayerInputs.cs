﻿using UnityEngine;
using UnityEngine.InputSystem;
using DYP;

namespace Saucy {
  [DisallowMultipleComponent]
  [RequireComponent(typeof(Player))]
  public class PlayerInputs : BaseInputDriver {
    public bool CanMove { get; set; } = true;
    public bool IsCrouching { get; set; } = false;
    public bool JumpPressed { get; set; }
    public bool JumpHolding { get; set; }
    public bool JumpReleased { get; set; }
    public bool IsJumping { get; set; }
    // private bool isAttacking;

    public bool ChangeJumpInputs = true; // TODO

    private Player player;
    private Vector2 movement;

    private void Awake () {
      player = GetComponent<Player>();
    }

    private void OnEnable () {
      CanMove = true;
      IsCrouching = false;
      JumpPressed = false;
      JumpHolding = false;
      JumpReleased = false;
      IsJumping = false;
      // isAttacking = false;
    }

    public void OnInputMove (InputAction.CallbackContext _context) {
      Vector2 _inputValues = _context.ReadValue<Vector2>();
      Vector2 _movement = new Vector2 {
        x = Mathf.RoundToInt(_inputValues.x),
        y = Mathf.RoundToInt(_inputValues.y)
      };

      movement = _movement;
    }


    public void OnInputJump (InputAction.CallbackContext _context) {
      if (!player.Data.SettingsJump.CanJump) {
        return;
      }

      if (ChangeJumpInputs) {
        if (_context.phase == InputActionPhase.Performed && !IsJumping) {
          // player.PlayerState.OnJump();
        } else if (_context.phase == InputActionPhase.Canceled) {
          // player.PlayerState.OnJumpCanceled();
        }
      } else {
        if (_context.phase == InputActionPhase.Performed && !IsJumping) {
          IsJumping = true;
          JumpPressed = true;
          JumpHolding = true;
        } else if (_context.phase == InputActionPhase.Canceled) {
          IsJumping = false;
          JumpReleased = true;
          JumpHolding = false;
        }
      }
    }

    public void OnInputCrouch (InputAction.CallbackContext _context) {
      if (!player.Data.CanCrouch) {
        return;
      }

      if (_context.phase == InputActionPhase.Performed) {
        // player.PlayerState.OnCrouch();
      }
    }

    private void Update () {
      UpdateInput(Time.deltaTime);
    }

    public override void UpdateInput (float timeStep) {
      if (!player.Controller.IsOnGround()) {
        if (!CanMove) {
          Horizontal = Mathf.Lerp(movement.x, 0f, Time.deltaTime);
        } else {
          Horizontal = movement.x;
        }
      } else {
        Horizontal = (CanMove) ? movement.x : 0f;
      }
      Vertical = (CanMove) ? movement.y : 0f;

      Jump = JumpPressed;
      // Dash = Input.GetButtonDown("Fire3");

      HoldingJump = JumpHolding;
      // HoldingDash = Input.GetButton("Fire3");

      ReleaseJump = JumpReleased;

      JumpPressed = false;
      JumpReleased = false;
    }
  }
}
