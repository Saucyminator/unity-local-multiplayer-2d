using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

// Script that will spawn a new player when a button on a device is pressed.
// https://docs.unity3d.com/Packages/com.unity.inputsystem@1.1/api/UnityEngine.InputSystem.InputSystem.html#UnityEngine_InputSystem_InputSystem_onAnyButtonPress

namespace Saucy {
  public class ListenForAnyDeviceInput : MonoBehaviour {
    [SerializeField] private GameObject playerPrefab = null; // We instantiate this GameObject to create a new player object. Expected to have a PlayerInput component in its hierarchy.
    [SerializeField] private string playerName = "Player 1";

    private IDisposable disposableEventListener; // We want to remove the event listener we install through InputSystem.onAnyButtonPress after we're done so remember it here.

    public UnityEvent OnEventJoined;

    private void OnEnable () {
      disposableEventListener = InputSystem.onAnyButtonPress.CallOnce(OnButtonPressed); // When enabled, we install our button press listener.
    }

    private void OnDisable () {
      disposableEventListener.Dispose(); // When disabled, we remove our button press listener.
    }

    private void OnButtonPressed (InputControl _button) {
      UnityEngine.InputSystem.InputDevice _device = _button.device;

      // Ignore presses on devices that are already used by a player.
      if (PlayerInput.FindFirstPairedToDevice(_device) != null) {
        return;
      }

      // Create a new player.
      PlayerInput _player = PlayerInput.Instantiate(playerPrefab, pairWithDevice: _device);
      _player.name = playerName;

      // If the player did not end up with a valid input setup, unjoin the player.
      if (_player.hasMissingRequiredDevices) {
        Destroy(_player);
      }

      // If we only want to join a single player, could uninstall our listener here or use CallOnce() instead of Call() when we set it up.
      OnEventJoined?.Invoke();
    }
  }
}
