﻿using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  public class Events : MonoBehaviour {
    public UnityEvent AwakeEvent;
    public UnityEvent OnEnableEvent;
    public UnityEvent ResetEvent;
    public UnityEvent StartEvent;
    public UnityEvent OnTriggerEnterEvent;
    public UnityEvent OnTriggerEnter2DEvent;
    public UnityEvent OnTriggerStayEvent;
    public UnityEvent OnTriggerStay2DEvent;
    public UnityEvent OnTriggerExitEvent;
    public UnityEvent OnTriggerExit2DEvent;
    public UnityEvent OnApplicationPauseEvent;
    public UnityEvent OnApplicationQuitEvent;
    public UnityEvent OnDisableEvent;
    public UnityEvent OnDestroyEvent;

    private void Awake () => AwakeEvent?.Invoke();
    private void OnEnable () => OnEnableEvent?.Invoke();
    private void Reset () => ResetEvent?.Invoke();
    private void Start () => StartEvent?.Invoke();
    private void OnTriggerEnter (Collider _other) => OnTriggerEnterEvent?.Invoke();
    private void OnTriggerEnter2D (Collider2D _other) => OnTriggerEnter2DEvent?.Invoke();
    private void OnTriggerStay (Collider _other) => OnTriggerStayEvent?.Invoke();
    private void OnTriggerStay2D (Collider2D _other) => OnTriggerStay2DEvent?.Invoke();
    private void OnTriggerExit (Collider _other) => OnTriggerExitEvent?.Invoke();
    private void OnTriggerExit2D (Collider2D _other) => OnTriggerExit2DEvent?.Invoke();
    private void OnApplicationPause (bool _pauseStatus) => OnApplicationPauseEvent?.Invoke();
    private void OnApplicationQuit () => OnApplicationQuitEvent?.Invoke();
    private void OnDisable () => OnDisableEvent?.Invoke();
    private void OnDestroy () => OnDestroyEvent?.Invoke();
  }
}
