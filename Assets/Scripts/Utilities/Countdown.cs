using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  public class Countdown : MonoBehaviour {
    [SerializeField] private float countdownTime = 1f;
    [SerializeField] private float interval = 1f;
    [Tooltip("Delay before starting the countdown.")] [SerializeField] private float delay = 0f;
    [Tooltip("By default if another StartTimer call is made it is ignored until the timer is finished. This overrides the setting and StartTimer can be called multiple times in a row to restart it.")] [SerializeField] private bool overrideTimer = false;
    [SerializeField] private bool endless = false;
    [Header("Start")] [SerializeField] private bool startTimerOnEnable = false;
    [SerializeField] private bool startTimerOnStart = false;
    [Header("Stop")] [SerializeField] private bool stopTimerOnDisable = false;
    [SerializeField] private bool stopTimerOnDestroy = false;

    public FloatEvent OnTimerStart;
    public FloatEvent OnTimerIntervalBegin;
    public FloatEvent OnTimerIntervalFinish;
    public FloatEvent OnTimerEnd;

    private bool isRunning;

    private void OnEnable () {
      if (startTimerOnEnable) {
        StartTimer();
      }
    }

    private void OnDisable () {
      if (isRunning && stopTimerOnDisable) {
        StopTimer();
      }
    }

    private void Start () {
      if (startTimerOnStart) {
        StartTimer();
      }
    }

    private void OnDestroy () {
      if (isRunning && stopTimerOnDestroy) {
        StopTimer();
      }
    }

    public void StartTimer () {
      StartTimer(countdownTime, interval, delay);
    }

    public void StartTimer (float _time, float _interval = 1f, float _delay = 0f) {
      if (isRunning && !overrideTimer) {
        return;
      }

      StopAllCoroutines();

      if (endless) {
        StartCoroutine(RunTimerEndless(_time, _interval, _delay));
      } else {
        StartCoroutine(RunTimer(_time, _interval, _delay));
      }
    }

    public void StopTimer () {
      StopAllCoroutines();
      isRunning = false;
    }

    private IEnumerator RunTimer (float _time, float _interval, float _delay) {
      yield return new WaitForSeconds(_delay);

      OnTimerStart?.Invoke(_time);

      while (_time > 0f) {
        OnTimerIntervalBegin?.Invoke(_time);

        yield return new WaitForSeconds(_interval);

        _time -= _interval;

        OnTimerIntervalFinish?.Invoke(_time);
      }

      OnTimerEnd?.Invoke(_time);
    }

    private IEnumerator RunTimerEndless (float _time, float _interval, float _delay) {
      while (true) {
        yield return RunTimer(_time, _interval, _delay);
      }
    }
  }
}
