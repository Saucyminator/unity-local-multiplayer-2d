using UnityEngine;
using UnityEditor;

namespace Saucy {
  public class ApplicationFunctions : MonoBehaviour {
    public void ApplicationQuit () {
#if UNITY_EDITOR
      // Application.Quit() does not work in the editor so UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
      Debug.Log("Quitting game");
      EditorApplication.isPlaying = false;
#else
      Application.Quit();
#endif
    }
  }
}
