using UnityEngine;

// Usage:

// Can also be attached to GameObjects in the scene.

// private WriteLog writeLog;
// private void Awake () {
//   writeLog = WriteLog.Instance;
//   writeLog.WriteToLog("Something to write");
// }

namespace Saucy {
  public class WriteLog : Singleton<WriteLog> {
    public bool writeDebugLogMessages = true;
    public string text;

    protected WriteLog () { }

    public void WriteToLog () {
      WriteToLog(text);
    }

    public void WriteToLog (string _text) {
      if (writeDebugLogMessages) {
        Debug.Log(_text);
      }
    }

    public void WriteToLog (float _text) {
      WriteToLog(_text.ToString());
    }

    public void WriteToLog (int _text) {
      WriteToLog(_text.ToString());
    }
  }
}
