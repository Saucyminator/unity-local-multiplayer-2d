﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  public class Timer : MonoBehaviour {
    [SerializeField] private float delay = 1f;
    [Tooltip("By default if another StartTimer call is made it is ignored until the timer is finished. This overrides the setting and StartTimer can be called multiple times in a row to restart it.")] [SerializeField] private bool overrideTimer = false;
    [SerializeField] private bool endless = false;
    [Header("Start")] [SerializeField] private bool startTimerOnEnable = false;
    [SerializeField] private bool startTimerOnStart = false;
    [Header("Stop")] [SerializeField] private bool stopTimerOnDisable = false;
    [SerializeField] private bool stopTimerOnDestroy = false;

    public UnityEvent OnTimerStart;
    public UnityEvent OnTimerEnd;

    private bool isRunning;

    private void OnEnable () {
      if (startTimerOnEnable) {
        StartTimer();
      }
    }

    private void OnDisable () {
      if (isRunning && stopTimerOnDisable) {
        StopTimer();
      }
    }

    private void Start () {
      if (startTimerOnStart) {
        StartTimer();
      }
    }

    private void OnDestroy () {
      if (isRunning && stopTimerOnDestroy) {
        StopTimer();
      }
    }

    public void StartTimer () {
      StartTimer(delay);
    }

    public void StartTimer (float _delay) {
      if (isRunning && !overrideTimer) {
        return;
      }

      StopAllCoroutines();

      if (endless) {
        StartCoroutine(RunTimerEndless(_delay));
      } else {
        StartCoroutine(RunTimer(_delay));
      }
    }

    public void StopTimer () {
      StopAllCoroutines();
      isRunning = false;
    }

    private IEnumerator RunTimer (float _delay) {
      isRunning = true;
      OnTimerStart?.Invoke();

      yield return new WaitForSeconds(_delay);

      isRunning = false;
      OnTimerEnd?.Invoke();
    }

    private IEnumerator RunTimerEndless (float _delay) {
      while (true) {
        yield return RunTimer(_delay);
      }
    }
  }
}
