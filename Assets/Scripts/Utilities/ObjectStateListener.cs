using UnityEngine;
using UnityEngine.Events;

// NOTE: Listens for when the target object is activated/deactivated.
// Credit: https://forum.unity.com/threads/event-for-when-a-gameobject-is-deactivated-activated.193980/#post-3630517

namespace Saucy {
  public class ObjectStateListener : MonoBehaviour {
    [SerializeField] private GameObject target = null;

    public UnityEvent onEventEnabled;
    public UnityEvent onEventDisabled;

    private ObjectStateExtensions.IStateInvoker invoker;

    private void OnEnable () {
      invoker = target.gameObject.GetStateListener();

      invoker.Enabled += Enabled;
      invoker.Disabled += Disabled;
    }

    private void OnDisable () {
      invoker.Enabled -= Enabled;
      invoker.Disabled -= Disabled;
    }

    private void Enabled () => onEventEnabled?.Invoke();
    private void Disabled () => onEventDisabled?.Invoke();
  }
}
