using UnityEngine;

namespace Saucy {
  public static class Utility {
    public static void SetLayerRecursively (GameObject _obj, string _newLayer, string _replacedLayer) {
      if (_obj == null || _obj.layer != LayerMask.NameToLayer(_replacedLayer)) {
        return;
      }

      _obj.layer = LayerMask.NameToLayer(_newLayer);

      foreach (Transform _child in _obj.transform) {
        if (_child == null) {
          continue;
        }

        SetLayerRecursively(_child.gameObject, _newLayer, _replacedLayer);
      }
    }

    public static T[] ShuffleArray<T> (T[] array, int seed) {
      System.Random prng = new System.Random(seed);

      for (int i = 0; i < array.Length - 1; i++) {
        int randomIndex = prng.Next(i, array.Length);
        T tempItem = array[randomIndex];
        array[randomIndex] = array[i];
        array[i] = tempItem;
      }

      return array;
    }

    public static Color RandomColor (Color[] _colors = null) {
      if (_colors == null) {
        _colors = new Color[] { Color.red, Color.blue, Color.green, Color.cyan, Color.gray, Color.magenta, Color.yellow, Color.white, Color.black };
      }

      return _colors[UnityEngine.Random.Range(0, _colors.Length)];
    }

    public static void EnableObject (ref GameObject _obj, bool _enable, bool _hasParent = true) {
      Collider[] colliders = _obj.GetComponentsInChildren<Collider>();
      Rigidbody rb = (_hasParent) ? _obj.GetComponentInParent<Rigidbody>() : _obj.GetComponent<Rigidbody>();
      rb.isKinematic = !_enable;

      foreach (Collider collider in colliders) {
        collider.enabled = _enable;
      }
    }
  }
}
