﻿using UnityEngine;

// This script is used to make UI elements face the player's camera

namespace Saucy {
  public class LookAtGameManagerCamera : MonoBehaviour {
    [SerializeField] private GameManager gameManager = null;

    private void LateUpdate () {
      if (gameManager.MainCamera == null) {
        return;
      }

      // Apply the rotation needed to look at the camera. Note, since pointing a UI text element
      // at the camera makes it appear backwards, we are actually pointing this object
      // directly *away* from the camera.
      transform.rotation = Quaternion.LookRotation(transform.position - gameManager.MainCamera.transform.position);
    }
  }
}
