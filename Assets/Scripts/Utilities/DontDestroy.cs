﻿using UnityEngine;

namespace Saucy {
  public class DontDestroy : MonoBehaviour {
    private void Awake () {
      DontDestroyOnLoad(gameObject);
    }
  }
}
