﻿using UnityEngine;

namespace Saucy {
  public class LayerChanger : MonoBehaviour {
    [SerializeField] private string replacedLayer = "";
    [SerializeField] private string newLayer = "";

    private void OnValidate () {
      if (gameObject.layer != LayerMask.NameToLayer(replacedLayer)) {
        Debug.LogError($"GameObject \"{gameObject.name}\" layer doesn't match with the replaced layer.");
      }
    }

    private void Awake () {
      if (gameObject.layer != LayerMask.NameToLayer(replacedLayer)) {
        Debug.LogError($"GameObject \"{gameObject.name}\" layer doesn't match with the replaced layer.");
      }
    }

    public void ChangeLayer () {
      Utility.SetLayerRecursively(gameObject, newLayer, replacedLayer);
    }

    public void ChangeLayer (string _newLayer) {
      Utility.SetLayerRecursively(gameObject, _newLayer, replacedLayer);
    }

    public void ChangeLayerToOriginal () {
      Utility.SetLayerRecursively(gameObject, replacedLayer, newLayer);
    }
  }
}
