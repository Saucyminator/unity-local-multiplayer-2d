﻿using UnityEngine;
using TMPro;

namespace Saucy {
  public class UISetText : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI label = null;
    [SerializeField] private string text = string.Empty;

    public void SetText () => SetText(text);
    public void SetText (string _text) => label.text = _text;
    public void SetText (int _text) => label.text = _text.ToString();
    public void SetText (float _text) => label.text = _text.ToString();
    public void SetText (bool _text) => label.text = _text.ToString();
  }
}
