﻿using UnityEngine;

namespace Saucy {
  public class Description : MonoBehaviour {
    [SerializeField] private bool printToLogOnEnable = true;
    [Tooltip("{0} = Name of the GameObject.\n{1} = The description.")] [SerializeField] private string format = "[{0}]: {1}";
    [TextArea(3, 15)] [SerializeField] private string description = "";

    private void OnEnable () {
      if (printToLogOnEnable) {
        Debug.Log(string.Format(format, name, description));
      }
    }
  }
}
