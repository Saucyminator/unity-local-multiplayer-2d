﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Saucy {
  public class VisibilityStatus : MonoBehaviour {
    public bool IsVisible { get; private set; } = true;

    [Tooltip("Objects to change visibility (its active status).")] [SerializeField] private List<GameObject> objectsToSetVisibility = new List<GameObject>();
    [Tooltip("True = Objects start visible (active).\nFalse = Objects start invisible (inactive).")] [SerializeField] private bool startVisible = true;
    public bool StartVisible {
      get { return startVisible; }
      set { startVisible = value; }
    }
    [Tooltip("True = Set objects visibility to Start Visible value.\nFalse = Objects keep its current visibility.")] [SerializeField] private bool overrideStartingVisibility = true;

    private Dictionary<GameObject, bool> originalVisibility = new Dictionary<GameObject, bool>();

    private void Awake () {
      objectsToSetVisibility.ForEach(_object => originalVisibility.Add(_object, _object.activeSelf));

      if (overrideStartingVisibility) {
        IsVisible = StartVisible;
        SetVisibility(IsVisible);
      }
    }

    public void ToggleVisibility () => objectsToSetVisibility.ForEach(_object => _object.SetActive(!_object.activeSelf));
    public void ShowObjects () => SetVisibility(true);
    public void HideObjects () => SetVisibility(false);

    public void SetVisibility (bool _visible) {
      IsVisible = _visible;

      objectsToSetVisibility.ForEach(_object => _object.SetActive(_visible));
    }

    public void ResetVisibility () => objectsToSetVisibility.ForEach(_object => _object.SetActive(originalVisibility.FirstOrDefault(_originalVisibilityObject => _originalVisibilityObject.Key == _object).Value));
  }
}
