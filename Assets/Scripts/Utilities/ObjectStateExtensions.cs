using System;
using UnityEngine;

// Credit: https://forum.unity.com/threads/event-for-when-a-gameobject-is-deactivated-activated.193980/#post-3630517

namespace Saucy {
  public static class ObjectStateExtensions {
    public static IStateInvoker GetStateListener (this GameObject _object) => _object.GetComponent<ObjectStateInvoker>() ?? _object.AddComponent<ObjectStateInvoker>();

    public interface IStateInvoker {
      event Action Enabled;
      event Action Disabled;
    }

    private class ObjectStateInvoker : MonoBehaviour, IStateInvoker {
      public event Action Enabled;
      public event Action Disabled;

      private void Awake () => hideFlags = HideFlags.DontSaveInBuild | HideFlags.HideInInspector;
      private void OnEnable () => TryInvoke(Enabled);
      private void OnDisable () => TryInvoke(Disabled);
      private void TryInvoke (Action action) => action?.Invoke();
    }
  }
}
