using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Saucy {
  [CreateAssetMenu(menuName = "GameManager")]
  public class GameManager : ScriptableObject {
    public Dictionary<int, Player> Players { get; private set; } = new Dictionary<int, Player>();

    public Player GetPlayer (int _playerIndex) => Players.FirstOrDefault(player => player.Value.PlayerIndex == _playerIndex).Value;
    public Player GetPlayer (PlayerInput _input) => Players.FirstOrDefault(player => player.Value.PlayerInput == _input).Value;
    public int GetPlayerIndex (Player _player) => Players.FirstOrDefault(player => player.Value == _player).Key;
    public int GetPlayerIndex (PlayerInput _input) => Players.FirstOrDefault(player => player.Value.PlayerInput == _input).Key;
    public bool IsPlayerReady (Player _player) => Players.FirstOrDefault(player => player.Value == _player).Value.IsReady;
    public bool IsPlayerReady (int _playerIndex) => Players.FirstOrDefault(player => player.Value.PlayerIndex == _playerIndex).Value.IsReady;
    public bool IsPlayerReady (PlayerInput _input) => Players.FirstOrDefault(player => player.Value.PlayerInput == _input).Value.IsReady;
    public Player GetHostPlayer () => Players.FirstOrDefault(player => player.Value.IsHost).Value;
    public bool IsPlayerHost (Player _player) => Players.FirstOrDefault(player => player.Value == _player).Value.IsHost;

    [Header("Player setup")] [Tooltip("Minimum players to be able to start the game.")] [Range(1, 8)] public int MinPlayers = 2;
    [Tooltip("Is NOT synced with PlayerInputManager's max player count (can't set it through code).")] [Range(1, 8)] public int MaxPlayers = 4;
    [Header("Characters")] [Tooltip("Available characters the players can choose.")] public List<DataCharacter> AvailableCharacters;
    public List<DataCharacter> SelectableCharacters { get; set; } // Selectable characters that has not yet been chosen by players.
    [Header("Colors")] [Tooltip("Available colors the players can choose.")] public List<DataColor> AvailableColors;
    public List<DataColor> SelectableColors { get; set; } // Selectable colors that has not yet been chosen by players.

    [Header("Listening on")] [SerializeField] private PlayerGameEvent eventPlayerReady = null;
    [SerializeField] private PlayerGameEvent eventPlayerUnready = null;
    [Header("Broadcasting on")] [SerializeField] private PlayerGameEvent onEventHostPlayerJoined = null;
    [SerializeField] private PlayerGameEvent onEventPlayerJoined = null;
    [SerializeField] private PlayerGameEvent onEventPlayerLeft = null;
    [SerializeField] private GameEvent onEventAllPlayersReady = null;
    [SerializeField] private GameEvent onEventPlayersAreNotReady = null;
    [SerializeField] private PlayerGameEvent onEventCharacterAndColorSelected = null;

    [Header("TODO")] public bool GameIsStarted;
    public bool GameIsPaused;
    public bool GameIsInLocalMultiplayerMenu;
    public float timeBeforeStarting = 2f;
    public Camera MainCamera;
    public List<Transform> StartingSpawnPoints;
    public Vector3 GetSpawnPoint (int _playerIndex) => (StartingSpawnPoints.Count > _playerIndex + 1) ? StartingSpawnPoints[_playerIndex].position : Vector3.zero;

    public UnityEvent OnBack;
    public UnityEvent OnLoadMainMenu;
    public UnityEvent OnLoadNextLevel;
    public UnityEvent OnGameStart;
    public IntEvent OnLevelChanged;
    public PlayerEvent OnGamePaused;
    public UnityEvent OnGameOver;

    private void OnValidate () {
      if (AvailableColors.Count < MaxPlayers) {
        Debug.LogError("GameManager data has less colors available than there is Max Players!\nFix by adding more colors to the \"AvailableColors\"-list.");
      }

      if (AvailableCharacters.Count < MaxPlayers) {
        Debug.LogError("GameManager data has less characters available than there is Max Players!\nFix by adding more characters to the \"AvailableCharacters\"-list.");
      }
    }

    public void Initialization () {
      GameIsStarted = false;
      GameIsPaused = false;
      GameIsInLocalMultiplayerMenu = false;

      Time.timeScale = 1f;

      Players.Clear();
      StartingSpawnPoints.Clear();

      SelectableColors = new List<DataColor>(AvailableColors);
      SelectableCharacters = new List<DataCharacter>(AvailableCharacters);
    }

    public void SubscribeToEvents () {
      eventPlayerReady.OnEventRaised += CheckIfAllPlayersAreReady;
      eventPlayerUnready.OnEventRaised += CheckIfAllPlayersAreReady;
    }

    public void UnsubscribeFromEvents () {
      eventPlayerReady.OnEventRaised -= CheckIfAllPlayersAreReady;
      eventPlayerUnready.OnEventRaised -= CheckIfAllPlayersAreReady;
    }

    public void AddPlayer (PlayerInput _playerInput) {
      Player _player = _playerInput.GetComponent<Player>();
      int _playerIndex = 0;

      // Adds a player with the lowest index regardless if there's any players already in game.
      for (int _index = 0; _index < Players.Count + 1; _index++) {
        if (Players.ContainsKey(_index)) {
          continue;
        }

        _playerIndex = _index;
      }

      _player.PlayerIndex = _playerIndex;
      _player.name = $"Player {_player.PlayerSlot}";

      if (Players.Count == 0) {
        // First player to join is the host.
        _player.IsHost = true;

        onEventHostPlayerJoined.Raise(_player);
      }

      Players.Add(_playerIndex, _player);
      _player.Setup(SelectableCharacters[_playerIndex], SelectableColors[_playerIndex]); // TODO: check is this throws an error if 3 players have ready-ied up and 4th joins, which character & color does it get?

      _player.gameObject.transform.position = GetSpawnPoint(_playerIndex);

      Debug.Log($"Added: Player {_player.PlayerSlot}");

      onEventPlayerJoined?.Raise(_player);
    }

    public void RemovePlayer (Player _player) => RemovePlayer(_player.PlayerInput);

    public void RemovePlayer (PlayerInput _playerInput) {
      if (_playerInput == null) {
        return;
      }

      Player _player = _playerInput.GetComponent<Player>();

      if (Players.ContainsValue(_player)) {
        Players.Remove(_player.PlayerIndex);

        Debug.Log($"Removed: Player {_player.PlayerSlot}");

        onEventPlayerLeft?.Raise(_player);

        Destroy(_player.gameObject);
      }
    }

    // TODO: Script is calling this when exiting editor Play mode because this function is hooked up to OnDisable(). Fix?
    public void RemoveAllPlayersExceptTheHost () {
      for (int _index = Players.Count - 1; _index >= 0; _index--) {
        if (Players[_index].IsHost) {
          continue;
        }

        RemovePlayer(Players[_index]);
      }

      SetReadyStatusForAllPlayers(false);
    }

    public void EnablePlayers () => Players.Values.ToList().ForEach(_player => _player.Enable());
    public void DisablePlayers () => Players.Values.ToList().ForEach(_player => _player.Disable());

    public void CheckIfAllPlayersAreReady (Player _) {
      Debug.Log($"check all ready players: {_.name}");

      if (Players.Count >= MinPlayers && Players.All(_player => _player.Value.IsReady)) {
        Debug.Log($"all players ready");
        onEventAllPlayersReady?.Raise();
      } else {
        Debug.Log($"not all players are ready");
        onEventPlayersAreNotReady?.Raise();
      }
    }

    public void SetReadyStatusForAllPlayers (bool _isReady) => Players.All(_player => _player.Value.IsReady = _isReady);

    public void GameStart () {
      if (!GameIsStarted) {
        Debug.Log($"--- Game start ---");
        GameIsStarted = true;

        OnGameStart?.Invoke();
      }
    }

    public IEnumerator StartGame () {
      Debug.Log("starting game");

      yield return new WaitForSeconds(timeBeforeStarting);

      Debug.Log("GO GO GO!");

      OnLoadNextLevel?.Invoke();
    }

    public void PauseGame () {
      PauseGame(null);
    }

    public void PauseGame (Player _playerInstigator) {
      Time.timeScale = 0f;
      GameIsPaused = true;

      OnGamePaused?.Invoke(_playerInstigator);
    }

    public void UnpauseGame () {
      UnpauseGame(null);
    }

    public void UnpauseGame (Player _playerInstigator) {
      Debug.Log($"Player who unpaused the game: {_playerInstigator}");

      Time.timeScale = 1f;
      GameIsPaused = false;
    }

    public void GameOver () {
      Debug.Log($"Game over!");

      OnGameOver?.Invoke();
    }

    public void SelectCharacterAndColor (Player _player, DataCharacter _character, DataColor _color) {
      SelectableCharacters.Remove(_character);
      SelectableColors.Remove(_color);

      onEventCharacterAndColorSelected?.Raise(_player);
    }

    public void UnselectCharacterAndColor (Player _player, DataCharacter _character, DataColor _color) {
      SelectableCharacters.Add(_character);
      SelectableCharacters = SelectableCharacters.OrderBy(character => character.Name).ToList();

      SelectableColors.Add(_color);
      SelectableColors = SelectableColors.OrderBy(color => color.Name).ToList();
    }

    public DataColor SelectNextAvailableColor (DataColor _currentColor) => SelectableColors.SkipWhile(x => x != _currentColor).Skip(1).DefaultIfEmpty(SelectableColors[0]).FirstOrDefault();
    public DataColor SelectPreviousAvailableColor (DataColor _currentColor) => SelectableColors.TakeWhile(x => x != _currentColor).DefaultIfEmpty(SelectableColors[SelectableColors.Count - 1]).LastOrDefault();

    public DataCharacter SelectNextAvailableCharacter (DataCharacter _currentCharacter) => SelectableCharacters.SkipWhile(x => x != _currentCharacter).Skip(1).DefaultIfEmpty(SelectableCharacters[0]).FirstOrDefault();
    public DataCharacter SelectPreviousAvailableCharacter (DataCharacter _currentCharacter) => SelectableCharacters.TakeWhile(x => x != _currentCharacter).DefaultIfEmpty(SelectableCharacters[SelectableCharacters.Count - 1]).LastOrDefault();
  }
}
