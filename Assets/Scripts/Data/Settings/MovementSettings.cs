﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DYP;
using UnityEngine.Serialization;

namespace Saucy {
  [System.Serializable]
  public class MovementSettings {
    [Range(0.1f, 16f)] public float Speed = 8f;
    [Range(0.1f, 2f)] public float AccelerationTimeAirborne = 0.1f;
    [Range(0.1f, 2f)] public float AccelerationTimeGrounded = 0.1f;
    public bool CanFallThroughOneWayPlatforms = true;
  }
}
