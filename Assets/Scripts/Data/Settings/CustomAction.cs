﻿using UnityEngine;
using DYP;

namespace Saucy {
  [System.Serializable]
  public class CustomAction {
    public BaseActionModule Module;

    private int m_ActionDir;
    public int ActionDir { get { return m_ActionDir; } }

    private float m_ActionTimer;
    private float m_PrevActionTimer;

    public void Start (int actionDir) {
      m_ActionDir = actionDir;
      m_PrevActionTimer = 0.0f;
      m_ActionTimer = 0.0f;
    }

    public void _Update (float timeStep) {
      m_PrevActionTimer = m_ActionTimer;
      m_ActionTimer += timeStep;

      if (m_ActionTimer > Module.ActionTime) {
        m_ActionTimer = Module.ActionTime;
      }
    }

    public Vector2 GetActionVelocity () {
      if (Module != null) {
        return Module.GetActionSpeed(m_PrevActionTimer, m_ActionTimer);
      } else {
        return Vector2.zero;
      }
    }

    public float GetActionProgress () {
      return Module.GetActionProgress(m_ActionTimer);
    }
  }
}
