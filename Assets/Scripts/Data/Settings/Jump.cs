﻿using UnityEngine;

namespace Saucy {
  [System.Serializable]
  public class Jump {
    public bool CanJump = true;
    public bool HasVariableJumpHeight = true;
    [Range(0, 10)] public int AirJumpAllowed = 1;
    [Range(0.1f, 10f)] public float MaxHeight = 3.5f;
    [Range(0.1f, 10f)] public float MinHeight = 1f;
    [Range(0.1f, 1f)] public float TimeToApex = 0.4f;

    [Space] [Range(0f, 10f)] public float OnLadderJumpForce = 0.4f;
    [Range(0.01f, 1f)] public float FallingJumpPaddingTime = 0.09f;
    [Range(0.1f, 1f)] public float WillJumpPaddingTime = 0.15f;
  }
}
