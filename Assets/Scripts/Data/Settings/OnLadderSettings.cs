﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DYP;

namespace Saucy {
  [Serializable]
  public class OnLadderSettings {
    public bool SnapToRestrictedArea = false;
    public bool ExitLadderOnGround = true;
    [Range(0.1f, 10f)] public float OnLadderSpeed = 4f;
    public bool LockFacingToRight = true;
  }
}
