﻿using DYP;

namespace Saucy {
  [System.Serializable]
  public class Dash {
    public BaseDashModule Module;

    private int m_DashDir;
    public int DashDir { get { return m_DashDir; } }      // 1: right, -1: left

    private float m_DashTimer;
    private float m_PrevDashTimer;

    public void Start (int dashDir, float timeStep) {
      m_DashDir = dashDir;
      m_PrevDashTimer = 0.0f;
      m_DashTimer = timeStep;
    }

    public void _Update (float timeStep) {
      m_PrevDashTimer = m_DashTimer;
      m_DashTimer += timeStep;

      if (m_DashTimer > Module.DashTime) {
        m_DashTimer = Module.DashTime;
      }
    }

    public float GetDashSpeed () {
      if (Module != null) {
        return Module.GetDashSpeed(m_PrevDashTimer, m_DashTimer);
      } else {
        return 0;
      }
    }

    public float GetDashProgress () {
      return Module.GetDashProgress(m_DashTimer);
    }
  }
}
