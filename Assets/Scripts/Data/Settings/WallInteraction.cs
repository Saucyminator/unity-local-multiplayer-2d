﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DYP;

namespace Saucy {
  [Serializable]
  public class WallInteraction {
    public bool CanWallSlide = true;
    public float WallStickTime = 0.15f;
    [HideInInspector] public float WallStickTimer = 0.0f;
    public float WallSlideSpeedLoss = 0.05f;
    public float WallSlidingSpeedMax = 2f;

    public bool CanWallClimb = false;
    [Range(0.1f, 4f)] public float WallClimbSpeed = 2f;

    public bool CanWallJump = true;
    public Vector2 ClimbForce = new Vector2(12f, 16f);
    public Vector2 OffForce = new Vector2(8f, 15f);
    public Vector2 LeapForce = new Vector2(18f, 17f);

    public bool CanGrabLedge = false;
    [Range(0.1f, 4f)] public float LedgeDetectionOffset = 0.1f;

    [HideInInspector] public int WallDirX = 0;
  }
}
