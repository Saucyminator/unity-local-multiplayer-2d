﻿using UnityEngine;

// TODO: use a STATS struct? string,int or just keep it like this

namespace Saucy {
  [CreateAssetMenu(menuName = "Data/Character")]
  public class DataCharacter : ScriptableObject {
    public string Name = "";
    public Sprite Avatar;

    [Range(0.1f, 60f)] public float MovementSpeed = 4f;

    [Tooltip("Duration of the attack animation.")] [Min(0f)] public float AttackAnimationLength;
    [Tooltip("Duration of the resurrect animation.")] [Min(0f)] public float ResurrectAnimationLength;

    public MovementSettings SettingsMovement;
    public OnLadderSettings SettingsOnLadder;
    public Jump SettingsJump;
    public WallInteraction SettingsWallInteraction;
    public Dash SettingsDash;
    public CustomAction SettingsCustomAction;
    public bool CanCrouch;

    [TextArea(5, 25)] public string DevDescription = "";
    public float Gravity => -(2 * SettingsJump.MaxHeight) / Mathf.Pow(SettingsJump.TimeToApex, 2);
    public float MaxJumpSpeed => Mathf.Abs(Gravity) * SettingsJump.TimeToApex;
    public float MinJumpSpeed => Mathf.Sqrt(2 * Mathf.Abs(Gravity) * SettingsJump.MinHeight);

    private void OnValidate () {
      if (Name == null || Name.Length == 0 || Name == "") {
        Name = name;
      }
    }
  }
}
