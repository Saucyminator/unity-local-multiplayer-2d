﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Saucy {
  [CreateAssetMenu(menuName = "Data/Color")]
  public class DataColor : ScriptableObject {
    public string Name = "";
    public Color Color = Color.white;
  }
}
