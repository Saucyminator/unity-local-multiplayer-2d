﻿using UnityEngine.Events;

namespace Saucy {
  [System.Serializable]
  public class IntEvent : UnityEvent<int> { }
}
