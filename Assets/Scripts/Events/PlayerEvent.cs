using UnityEngine.Events;

// player

namespace Saucy {
  [System.Serializable]
  public class PlayerEvent : UnityEvent<Player> { }
}
