using UnityEngine.Events;

namespace Saucy {
  [System.Serializable]
  public class FloatEvent : UnityEvent<float> { }
}
