using UnityEngine.Events;

namespace Saucy {
  [System.Serializable]
  public class BoolEvent : UnityEvent<bool> { }
}
