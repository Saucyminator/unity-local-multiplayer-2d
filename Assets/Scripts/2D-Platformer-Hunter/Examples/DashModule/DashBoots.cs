﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DYP;

public class DashBoots : MonoBehaviour {
  [SerializeField]
  private BaseDashModule m_Module = null;

  [SerializeField]
  private PlayerMovementController2D m_EquipTarget = null;

  public void OnEnable () {
    Equip();
  }

  public void OnDisable () {
    Unequip();
  }

  public void Equip () {
    m_EquipTarget.ChangeDashModule(m_Module);
  }

  public void Unequip () {
    m_EquipTarget.ChangeDashModule(null);
  }
}
