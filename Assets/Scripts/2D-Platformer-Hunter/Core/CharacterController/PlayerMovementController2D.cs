﻿using System;
using UnityEngine;
using Saucy;

/*
 * -- PlayerCharacterController2D
 *
 *  Handles Gravity, Jump and Horizontal Movement
 */

namespace DYP {
  [RequireComponent(typeof(CharacterMotor2D))]
  public class PlayerMovementController2D : MonoBehaviour {
    [System.Serializable]
    class InputBuffer {
      public Vector2 Input = Vector2.zero;

      public bool IsJumpPressed = false;
      public bool IsJumpHeld = false;
      public bool IsJumpReleased = false;
      public bool IsDashPressed = false;
      public bool IsDashHeld = false;
    }

    [System.Serializable]
    class OnLadderState {
      public bool IsInLadderArea = false;
      public Bounds Area = new Bounds(Vector3.zero, Vector3.zero);
      public Bounds BottomArea = new Bounds(Vector3.zero, Vector3.zero);
      public Bounds TopArea = new Bounds(Vector3.zero, Vector3.zero);
      public LadderZone AreaZone = LadderZone.Bottom;

      public bool HasRestrictedArea = false;
      public Bounds RestrictedArea = new Bounds();
      public Vector2 RestrictedAreaTopRight = new Vector2(Mathf.Infinity, Mathf.Infinity);
      public Vector2 RestrictedAreaBottomLeft = new Vector2(-Mathf.Infinity, -Mathf.Infinity);
    }

    private CharacterMotor2D m_Motor;
    public Collider2D MotorCollider => m_Motor.Collider2D;

    private BaseInputDriver m_InputDriver;
    private InputBuffer m_InputBuffer = new InputBuffer();

    private DataCharacter Data;

    private MotorState m_MotorState;

    // private float m_Gravity;
    private bool m_ApplyGravity = true;

    // private float Character.MaxJumpSpeed;
    // private float m_MinJumpSpeed;

    private int m_AirJumpCounter = 0;
    private int m_WillJumpPaddingFrame = -1;
    private int m_FallingJumpPaddingFrame = -1;

    private float m_CurrentTimeStep = 0;

    private int m_FacingDirection = 1;
    public int FacingDirection {
      get { return m_FacingDirection; }
      private set {
        int oldFacing = m_FacingDirection;
        m_FacingDirection = value;

        if (m_FacingDirection != oldFacing) {
          OnFacingFlip(m_FacingDirection);
        }
      }
    }

    public BaseDashModule DashModule { get { return Data.SettingsDash.Module; } private set { Data.SettingsDash.Module = value; } }

    public BaseActionModule ActionModule { get { return Data.SettingsCustomAction.Module; } private set { Data.SettingsCustomAction.Module = value; } }

    private OnLadderState m_OnLadderState = new OnLadderState();

    [Header("State")]
    private Vector3 m_Velocity;
    public Vector3 InputVelocity => m_Velocity;

    public float MovementSpeed { get { return Data.SettingsMovement.Speed; } set { Data.SettingsMovement.Speed = value; } }

    private float m_VelocityXSmoothing;

    // Action
    public event Action<MotorState, MotorState> OnMotorStateChanged = delegate { };

    public event System.Action OnJump = delegate { };                // on all jump! // OnEnterStateJump
    public event System.Action OnJumpEnd = delegate { };             // on jump -> falling  // OnLeaveStateJump

    public event System.Action OnNormalJump = delegate { };          // on ground jump
    public event System.Action OnLedgeJump = delegate { };           // on ledge jump
    public event System.Action OnLadderJump = delegate { };          // on ladder jump
    public event System.Action OnAirJump = delegate { };             // on air jump
    public event System.Action<Vector2> OnWallJump = delegate { };   // on wall jump

    public event System.Action<int> OnDash = delegate { };           // int represent dash direction
    public event System.Action<float> OnDashStay = delegate { };     // float represent action progress
    public event System.Action OnDashEnd = delegate { };

    public event System.Action<int> OnWallSliding = delegate { };    // int represnet wall direction: 1 -> right, -1 -> left
    public event System.Action OnWallSlidingEnd = delegate { };

    public event System.Action<int> OnLedgeGrabbing = delegate { };
    public event System.Action OnLedgeGrabbingEnd = delegate { };

    public event System.Action OnLanded = delegate { };              // on grounded

    public event System.Action<MotorState> OnResetJumpCounter = delegate { };
    public event System.Action<int> OnFacingFlip = delegate { };

    public event System.Action<int> OnAction = delegate { };
    public event System.Action<float> OnActionStay = delegate { };
    public event System.Action OnActionEnd = delegate { };

    // Condition
    public Func<bool> CanAirJumpFunc = null;

    #region Monobehaviour

    private void Reset () {
      m_Motor = GetComponent<CharacterMotor2D>();
    }

    private void Awake () {
      m_Motor = GetComponent<CharacterMotor2D>();
      m_InputDriver = GetComponent<BaseInputDriver>();

      if (m_InputDriver == null) {
        Debug.LogWarning("An InputDriver is needed for a BasicCharacterController2D");
      }
    }

    private void Start () {
      Init();
    }

    private void FixedUpdate () {
      _Update(Time.fixedDeltaTime);
    }

    private void Update () {
      readInput(Time.deltaTime);
    }

    private void OnDrawGizmosSelected () {
      if (IsInLadderArea()) {
        Gizmos.color = new Color(1.0f, 0.5f, 0.5f, 0.5f);
        Gizmos.DrawCube(m_OnLadderState.Area.center, m_OnLadderState.Area.extents * 2);

        Gizmos.color = (m_OnLadderState.AreaZone == LadderZone.Top) ? new Color(1.0f, 0.92f, 0.016f, 1.0f) : Color.white;
        Gizmos.DrawWireCube(m_OnLadderState.TopArea.center, m_OnLadderState.TopArea.extents * 2);

        Gizmos.color = (m_OnLadderState.AreaZone == LadderZone.Bottom) ? new Color(1.0f, 0.92f, 0.016f, 1.0f) : Color.white;
        Gizmos.DrawWireCube(m_OnLadderState.BottomArea.center, m_OnLadderState.BottomArea.extents * 2);


        if (IsRestrictedOnLadder()) {
          Gizmos.color = Color.red;
          Gizmos.DrawWireCube(m_OnLadderState.RestrictedArea.center, m_OnLadderState.RestrictedArea.extents * 2);
        }
      }

      // draw ledge grabbing gizmos
      if (m_Motor != null) {
        var boundingBox = MotorCollider.bounds;

        Vector2 origin = Vector2.zero;
        origin.y = boundingBox.max.y + Data.SettingsWallInteraction.LedgeDetectionOffset;

        float distance = Data.SettingsWallInteraction.LedgeDetectionOffset * 2;
        float speedY = -m_Velocity.y * m_CurrentTimeStep;
        distance = (speedY > distance) ? speedY : distance;

        Vector2 hitPoint = Vector2.zero;

        // right ledge line
        origin.x = boundingBox.max.x + Data.SettingsWallInteraction.LedgeDetectionOffset;

        bool rightLedge = CheckIfAtLedge(1, ref hitPoint);

        Gizmos.color = (rightLedge) ? Color.blue : Color.red;
        Gizmos.DrawLine(origin, origin + Vector2.down * distance);

        if (rightLedge) {
          Gizmos.DrawSphere(hitPoint, 0.03f);
        }


        // left ledge line
        origin.x = boundingBox.min.x - Data.SettingsWallInteraction.LedgeDetectionOffset;

        bool leftLedge = CheckIfAtLedge(-1, ref hitPoint);

        Gizmos.color = (leftLedge) ? Color.blue : Color.red;
        Gizmos.DrawLine(origin, origin + Vector2.down * distance);

        if (leftLedge) {
          Gizmos.DrawSphere(hitPoint, 0.03f);
        }
      }

      // draw center
      Gizmos.color = Color.blue;
      Gizmos.DrawWireSphere(GetComponent<Collider2D>().bounds.center, .5f);
    }

    #endregion

    public void Setup (Player _player) {
      Data = _player.Data;
    }

    public void SetFrozen (bool freeze) {
      if (freeze) {
        m_Velocity.x = 0;
        changeState(MotorState.Frozen);
      } else {
        if (IsOnGround()) {
          changeState(MotorState.OnGround);
        } else {
          changeState(MotorState.Falling);
        }
      }
    }

    public bool IsState (MotorState state) {
      return m_MotorState == state;
    }

    public bool IsInLadderArea () {
      return m_OnLadderState.IsInLadderArea;
    }

    public bool IsInLadderTopArea () {
      return m_OnLadderState.IsInLadderArea && m_OnLadderState.AreaZone == LadderZone.Top;
    }

    public bool IsOnGround () {
      return m_Motor.Collisions.Below;
    }

    public bool IsInAir () {
      return !m_Motor.Collisions.Below && !m_Motor.Collisions.Left && !m_Motor.Collisions.Right; //IsState(MotorState.Jumping) || IsState(MotorState.Falling);
    }

    public bool IsAgainstWall () {
      if (m_Motor.Collisions.Left) {
        float leftWallAngle = Vector2.Angle(m_Motor.Collisions.LeftNormal, Vector2.right);
        if (leftWallAngle < 0.01f) {
          return true;
        }
      }

      if (m_Motor.Collisions.Right) {
        float rightWallAngle = Vector2.Angle(m_Motor.Collisions.RightNormal, Vector2.left);
        if (rightWallAngle < 0.01f) {
          return true;
        }
      }

      return false;
    }

    public bool CheckIfAtLedge (int wallDirX, ref Vector2 ledgePoint) {
      // first raycast down, then check overlap
      var boundingBox = MotorCollider.bounds;

      Vector2 origin = Vector2.zero;
      origin.y = boundingBox.max.y + Data.SettingsWallInteraction.LedgeDetectionOffset;

      // right wall
      if (wallDirX == 1) {
        origin.x = boundingBox.max.x + Data.SettingsWallInteraction.LedgeDetectionOffset;
      }
      // left wall
      else if (wallDirX == -1) {
        origin.x = boundingBox.min.x - Data.SettingsWallInteraction.LedgeDetectionOffset;
      }

      float distance = Data.SettingsWallInteraction.LedgeDetectionOffset * 2;
      float speedY = -m_Velocity.y * m_CurrentTimeStep;
      distance = (speedY > distance) ? speedY : distance;

      RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.down, distance, m_Motor.Raycaster.CollisionLayer);

      ledgePoint = hit.point;

      if (hit.collider != null) {
        Bounds overlapBox = new Bounds(
            new Vector3(ledgePoint.x, ledgePoint.y) + Vector3.up * Data.SettingsWallInteraction.LedgeDetectionOffset,
            Vector3.one * Data.SettingsWallInteraction.LedgeDetectionOffset);

        Collider2D col = Physics2D.OverlapArea(overlapBox.min, overlapBox.max);
        return (col == null);
      } else {
        return false;
      }
    }

    public bool CanAirJump () {
      if (CanAirJumpFunc != null) {
        return CanAirJumpFunc();
      } else {
        return m_AirJumpCounter < Data.SettingsJump.AirJumpAllowed;
      }
    }

    public void ChangeDashModule (BaseDashModule module, bool disableDashingState = false) {
      if (module != null) {
        DashModule = module;

        if (disableDashingState) {
          changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
        }
      } else {
        DashModule = null;
        changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
      }
    }

    public void ChangeActionModule (BaseActionModule module, bool disableActionState = false) {
      if (module != null) {
        ActionModule = module;

        if (disableActionState) {
          changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
        }
      } else {
        ActionModule = null;
        changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
      }
    }

    public void LadderAreaEnter (Bounds area, float topAreaHeight, float bottomAreaHeight) {
      m_OnLadderState.IsInLadderArea = true;
      m_OnLadderState.Area = area;

      m_OnLadderState.TopArea = new Bounds(
          new Vector3(area.center.x, area.center.y + area.extents.y - topAreaHeight / 2, 0),
          new Vector3(area.size.x, topAreaHeight, 100)
      );

      m_OnLadderState.BottomArea = new Bounds(
          new Vector3(area.center.x, area.center.y - area.extents.y + bottomAreaHeight / 2, 0),
          new Vector3(area.size.x, bottomAreaHeight, 100)
      );
    }

    public void LadderAreaExit () {
      m_OnLadderState.IsInLadderArea = false;

      m_OnLadderState.Area = new Bounds(Vector3.zero, Vector3.zero);
      m_OnLadderState.TopArea = new Bounds(Vector3.zero, Vector3.zero);
      m_OnLadderState.BottomArea = new Bounds(Vector3.zero, Vector3.zero);

      if (IsState(MotorState.OnLadder)) {
        exitLadderState();
      }
    }

    public void SetLadderRestrictedArea (Bounds b, bool isTopIgnored = false) {
      m_OnLadderState.HasRestrictedArea = true;

      m_OnLadderState.RestrictedArea = b;
      m_OnLadderState.RestrictedAreaTopRight = b.center + b.extents;
      m_OnLadderState.RestrictedAreaBottomLeft = b.center - b.extents;

      if (isTopIgnored) {
        m_OnLadderState.RestrictedAreaTopRight.y = Mathf.Infinity;
      }
    }

    public void SetLadderZone (LadderZone zone) {
      m_OnLadderState.AreaZone = zone;
    }

    public void ClearLadderRestrictedArea () {
      m_OnLadderState.HasRestrictedArea = false;
    }

    public bool IsRestrictedOnLadder () {
      return m_OnLadderState.HasRestrictedArea;
    }

    public void Init () {
      // S = V0 * t + a * t^2 * 0.5
      // h = V0 * t + g * t^2 * 0.5
      // h = g * t^2 * 0.5
      // g = h / (t^2*0.5)

      // m_Gravity = -Character.m_Jump.MaxHeight / (Character.m_Jump.TimeToApex * Character.m_Jump.TimeToApex * 0.5f);
      // Character.MaxJumpSpeed = Mathf.Abs(m_Gravity) * Character.m_Jump.TimeToApex;
      // m_MinJumpSpeed = Mathf.Sqrt(2 * Mathf.Abs(m_Gravity) * Character.m_Jump.MinHeight);
      m_AirJumpCounter = 0;

      m_Motor.OnMotorCollisionEnter2D += onMotorCollisionEnter2D;
      m_Motor.OnMotorCollisionStay2D += onMotorCollisionStay2D;
    }

    private void readInput (float timeStep) {
      // Update input buffer
      m_InputBuffer.Input = new Vector2(m_InputDriver.Horizontal, m_InputDriver.Vertical);

      m_InputBuffer.IsJumpPressed = m_InputDriver.Jump;

      if (m_InputBuffer.IsJumpPressed) {
        m_WillJumpPaddingFrame = calculateFramesFromTime(Data.SettingsJump.WillJumpPaddingTime, timeStep);
      }

      m_InputBuffer.IsJumpHeld = m_InputDriver.HoldingJump;
      m_InputBuffer.IsJumpReleased = m_InputDriver.ReleaseJump;

      m_InputBuffer.IsDashPressed = m_InputDriver.Dash;
      m_InputBuffer.IsDashHeld = m_InputDriver.HoldingDash;
    }

    public void _Update (float timeStep) {
      m_CurrentTimeStep = timeStep;

      updateTimers(timeStep);

      updateState(timeStep);

      // check padding frame
      if (m_WillJumpPaddingFrame >= 0) {
        m_InputBuffer.IsJumpPressed = true;
      }

      // read input from input driver
      Vector2 input = m_InputBuffer.Input;

      Vector2Int rawInput = Vector2Int.zero;

      if (input.x > 0.0f)
        rawInput.x = 1;
      else if (input.x < 0.0f)
        rawInput.x = -1;

      if (input.y > 0.0f)
        rawInput.y = 1;
      else if (input.y < 0.0f)
        rawInput.y = -1;

      // check which side of character is collided
      int wallDirX = 0;

      if (m_Motor.Collisions.Right) {
        wallDirX = 1;
      } else if (m_Motor.Collisions.Left) {
        wallDirX = -1;
      }

      Data.SettingsWallInteraction.WallDirX = wallDirX;

      // check if want dashing
      if (m_InputBuffer.IsDashPressed) {
        startDash(rawInput.x, timeStep);
      }

      // check if want climbing ladder
      if (IsInLadderArea()) {
        if (IsInLadderTopArea()) {
          if (rawInput.y < 0) {
            enterLadderState();
          }
        } else {
          if (rawInput.y > 0) {
            enterLadderState();
          }
        }
      }

      // dashing state
      if (IsState(MotorState.Dashing)) {
        m_Velocity.x = Data.SettingsDash.DashDir * Data.SettingsDash.GetDashSpeed(); //getDashSpeed();

        if (!IsOnGround() && DashModule.UseGravity)
          m_Velocity.y = 0;

        if (DashModule.ChangeFacing) {
          FacingDirection = (int) Mathf.Sign(m_Velocity.x);
        }

        if (DashModule.UseCollision) {
          m_Motor.Move(m_Velocity * timeStep, false);
        }
        // teleport, if there is no obstacle on the target position -> teleport, or use collision to find the closest teleport position
        else {
          bool cannotTeleportTo = Physics2D.OverlapBox(
              m_Motor.Collider2D.bounds.center + m_Velocity * timeStep,
              m_Motor.Collider2D.bounds.size,
              0.0f,
              m_Motor.Raycaster.CollisionLayer);

          if (!cannotTeleportTo) {
            m_Motor.transform.Translate(m_Velocity * timeStep);
          } else {
            m_Motor.Move(m_Velocity * timeStep, false);
          }
        }
      }

      // on custom action
      else if (IsState(MotorState.CustomAction)) {
        //m_Velocity.x = m_ActionState.GetActionVelocity();

        //if (!IsGrounded() && DashModule.UseGravity)
        //    m_Velocity.y = 0;
      }

      // on ladder state
      else if (IsState(MotorState.OnLadder)) {
        m_Velocity = input * Data.SettingsOnLadder.OnLadderSpeed;

        // jump if jump input is true
        if (m_InputBuffer.IsJumpPressed) {
          startJump(rawInput, wallDirX);
        }

        if (Data.SettingsOnLadder.LockFacingToRight) {
          FacingDirection = 1;
        } else {
          if (m_Velocity.x != 0.0f)
            FacingDirection = (int) Mathf.Sign(m_Velocity.x);
        }

        //m_Motor.Move(m_Velocity * timeStep, false);

        // dont do collision detection
        if (m_OnLadderState.HasRestrictedArea) {
          // outside right, moving right disallowed
          if (m_Motor.transform.position.x > m_OnLadderState.RestrictedAreaTopRight.x) {
            if (m_Velocity.x > 0.0f) {
              m_Velocity.x = 0.0f;
            }
          }

          // outside left, moving left disallowed
          if (m_Motor.transform.position.x < m_OnLadderState.RestrictedAreaBottomLeft.x) {
            if (m_Velocity.x < 0.0f) {
              m_Velocity.x = 0.0f;
            }
          }

          // outside up, moving up disallowed
          if (m_Motor.transform.position.y > m_OnLadderState.RestrictedAreaTopRight.y) {
            if (m_Velocity.y > 0.0f) {
              m_Velocity.y = 0.0f;
            }
          }

          // outside down, moving down disallowed
          if (m_Motor.transform.position.y < m_OnLadderState.RestrictedAreaBottomLeft.y) {
            if (m_Velocity.y < 0.0f) {
              m_Velocity.y = 0.0f;
            }
          }
        }

        Vector2 targetPos = m_Motor.transform.position + m_Velocity * timeStep;

        Vector2 currPos = m_Motor.transform.position;

        m_Motor.Move(targetPos - currPos);
        //m_Motor.transform.position += m_Velocity * timeStep;

        if (m_OnLadderState.HasRestrictedArea) {
          targetPos.x = Mathf.Clamp(targetPos.x, m_OnLadderState.RestrictedAreaBottomLeft.x, m_OnLadderState.RestrictedAreaTopRight.x);
          targetPos.y = Mathf.Clamp(targetPos.y, m_OnLadderState.RestrictedAreaBottomLeft.y, m_OnLadderState.RestrictedAreaTopRight.y);

          // restricted in x axis
          if (targetPos.x != m_Motor.transform.position.x) {
            if (!Data.SettingsOnLadder.SnapToRestrictedArea) {
              targetPos.x = Mathf.Lerp(m_Motor.transform.position.x, targetPos.x, 0.25f);
            }
          }

          // restricted in y axis
          if (targetPos.y != m_Motor.transform.position.y) {
            if (!Data.SettingsOnLadder.SnapToRestrictedArea) {
              targetPos.y = Mathf.Lerp(m_Motor.transform.position.y, targetPos.y, 0.25f);
            }
          }

          m_Motor.transform.position = targetPos;
        }
      } else if (IsState(MotorState.Frozen)) {
        // Reset gravity if collision happened in y axis
        if (m_Motor.Collisions.Above) {
          //Debug.Log("Reset Vec Y");
          m_Velocity.y = 0;
        } else if (m_Motor.Collisions.Below) {
          // falling downward
          if (m_Velocity.y < 0.0f) {
            m_Velocity.y = 0;
          }
        }

        if (m_ApplyGravity) {
          float gravity = Data.Gravity;
          m_Velocity.y += gravity * timeStep;
        }
        m_Motor.Move(m_Velocity * timeStep, false);
      } else // other state
        {
        // fall through one way platform
        if (Data.SettingsMovement.CanFallThroughOneWayPlatforms && m_InputBuffer.IsJumpHeld && rawInput.y < 0) {
          Debug.Log($"falling through");
          m_Motor.FallThrough();
          changeState(MotorState.Falling);
        }

        // setup velocity.x based on input
        float targetVecX = input.x * Data.SettingsMovement.Speed;

        // smooth x direction motion
        if (IsOnGround()) {
          m_Velocity.x = targetVecX;
          m_VelocityXSmoothing = targetVecX;
        } else {
          m_Velocity.x = Mathf.SmoothDamp(m_Velocity.x, targetVecX, ref m_VelocityXSmoothing, Data.SettingsMovement.AccelerationTimeAirborne);
        }
        /*
        m_Velocity.x = Mathf.SmoothDamp(m_Velocity.x, targetVecX, ref m_VelocityXSmoothing,
            (m_Motor.Collisions.Below) ? Character.m_MovementSettings.AccelerationTimeGrounded : Character.m_MovementSettings.AccelerationTimeAirborne);
        */

        // check wall sticking and jumping
        bool isStickToWall = false;
        bool isGrabbingLedge = false;
        Vector2 ledgePos = Vector2.zero;

        if (IsAgainstWall()) {
          // ledge grabbing logic
          if (Data.SettingsWallInteraction.CanGrabLedge) {
            if (CheckIfAtLedge(wallDirX, ref ledgePos)) {
              if (!IsState(MotorState.OnLedge)) {
                if (m_Velocity.y < 0 && wallDirX == rawInput.x) {
                  isGrabbingLedge = true;
                  m_Velocity.y = 0;

                  float adjustY = ledgePos.y - MotorCollider.bounds.max.y;

                  m_Motor.transform.position += Vector3.up * adjustY;
                }
              } else {
                isGrabbingLedge = true;
              }
            }

            if (isGrabbingLedge) {
              changeState(MotorState.OnLedge);

              m_AirJumpCounter = 0;
              OnResetJumpCounter.Invoke(MotorState.OnLedge);

              // check if still sticking to wall
              if (Data.SettingsWallInteraction.WallStickTimer > 0.0f) {
                m_VelocityXSmoothing = 0;
                m_Velocity.x = 0;

                // leaving wall
                if (rawInput.x == -wallDirX) {
                  Data.SettingsWallInteraction.WallStickTimer -= timeStep;
                }
                // not leaving wall
                else {
                  Data.SettingsWallInteraction.WallStickTimer = Data.SettingsWallInteraction.WallStickTime;
                }
              } else {
                changeState(MotorState.Falling);
                Data.SettingsWallInteraction.WallStickTimer = Data.SettingsWallInteraction.WallStickTime;
              }
            }

          }

          // wall sliding logic
          if (!isGrabbingLedge && Data.SettingsWallInteraction.CanWallSlide) {
            // is OnGround, press against wall and jump
            if (IsOnGround()) {
              if (!IsState(MotorState.WallSliding)) {
                if (Data.SettingsWallInteraction.CanWallClimb) {
                  if (rawInput.x == wallDirX && m_InputBuffer.IsJumpPressed) {
                    isStickToWall = true;

                    consumeJumpPressed();
                  }
                }
              }
            }
            // is not OnGround, press against wall or was wallsliding
            else {
              if (IsState(MotorState.WallSliding) || rawInput.x == wallDirX) {
                isStickToWall = true;
              }
            }

            if (isStickToWall) {
              changeState(MotorState.WallSliding);

              m_AirJumpCounter = 0;
              OnResetJumpCounter.Invoke(MotorState.WallSliding);

              // check if still sticking to wall
              if (Data.SettingsWallInteraction.WallStickTimer > 0.0f) {
                m_VelocityXSmoothing = 0;
                m_Velocity.x = 0;

                if (rawInput.x != wallDirX && rawInput.x != 0) {
                  Data.SettingsWallInteraction.WallStickTimer -= timeStep;

                  if (Data.SettingsWallInteraction.WallStickTimer < 0.0f) {
                    changeState(MotorState.Falling);
                    Data.SettingsWallInteraction.WallStickTimer = Data.SettingsWallInteraction.WallStickTime;
                  }
                } else {
                  Data.SettingsWallInteraction.WallStickTimer = Data.SettingsWallInteraction.WallStickTime;
                }
              } else {
                changeState(MotorState.Falling);
                Data.SettingsWallInteraction.WallStickTimer = Data.SettingsWallInteraction.WallStickTime;
              }
            }
          }
        }

        // Reset gravity if collision happened in y axis
        if (m_Motor.Collisions.Above) {
          //Debug.Log("Reset Vec Y");
          m_Velocity.y = 0;
        } else if (m_Motor.Collisions.Below) {
          // falling downward
          if (m_Velocity.y < 0.0f) {
            m_Velocity.y = 0;
          }
        }

        // jump if jump input is true
        if (m_InputBuffer.IsJumpPressed /* && rawInput.y >= 0 */) {
          startJump(rawInput, wallDirX);
        }

        // variable jump height based on user input
        if (Data.SettingsJump.HasVariableJumpHeight) {
          if (!m_InputBuffer.IsJumpHeld /* && rawInput.y >= 0 */) {
            if (m_Velocity.y > Data.MinJumpSpeed)
              m_Velocity.y = Data.MinJumpSpeed;
          }
        }

        if (m_ApplyGravity) {
          float gravity = Data.Gravity;
          if (IsState(MotorState.WallSliding) && m_Velocity.y < 0) {
            gravity *= Data.SettingsWallInteraction.WallSlideSpeedLoss;
          }

          m_Velocity.y += gravity * timeStep;
        }

        // control ledge grabbing speed
        if (isGrabbingLedge) {
          if (IsState(MotorState.OnLedge)) {
            if (m_Velocity.y < 0) {
              m_Velocity.y = 0;
            }
          }

          FacingDirection = (m_Motor.Collisions.Right) ? 1 : -1;
        }
        // control wall sliding speed
        else if (isStickToWall) {
          if (Data.SettingsWallInteraction.CanWallClimb) {
            if (IsState(MotorState.WallSliding)) {
              m_Velocity.y = input.y * Data.SettingsWallInteraction.WallClimbSpeed;
            }
          } else {
            if (m_Velocity.y < -Data.SettingsWallInteraction.WallSlidingSpeedMax) {
              m_Velocity.y = -Data.SettingsWallInteraction.WallSlidingSpeedMax;
            }
          }

          FacingDirection = (m_Motor.Collisions.Right) ? 1 : -1;
        } else {
          if (m_Velocity.x != 0.0f)
            FacingDirection = (int) Mathf.Sign(m_Velocity.x);
        }

        m_Motor.Move(m_Velocity * timeStep, false);
      }

      // check ladder area
      if (IsInLadderArea()) {
        if (m_OnLadderState.BottomArea.Contains(m_Motor.Collider2D.bounds.center)) {
          m_OnLadderState.AreaZone = LadderZone.Bottom;
        } else if (m_OnLadderState.TopArea.Contains(m_Motor.Collider2D.bounds.center)) {
          m_OnLadderState.AreaZone = LadderZone.Top;
        } else if (m_OnLadderState.Area.Contains(m_Motor.Collider2D.bounds.center)) {
          m_OnLadderState.AreaZone = LadderZone.Middle;
        }
      }
    }


    private void updateTimers (float timeStep) {
      if (IsState(MotorState.Dashing)) {
        Data.SettingsDash._Update(timeStep);
      }

      if (IsState(MotorState.CustomAction)) {
        Data.SettingsCustomAction._Update(timeStep);
      }


      if (m_FallingJumpPaddingFrame >= 0) m_FallingJumpPaddingFrame--;
      if (m_WillJumpPaddingFrame >= 0) m_WillJumpPaddingFrame--;
    }

    private void updateState (float timeStep) {
      if (IsState(MotorState.Dashing)) {
        OnDashStay(Data.SettingsDash.GetDashProgress());

        if (Data.SettingsDash.GetDashProgress() >= 1.0f) {
          endDash();
        }
      }

      if (IsState(MotorState.Dashing)) {
        return;
      }

      if (IsState(MotorState.CustomAction)) {
        OnActionStay(Data.SettingsCustomAction.GetActionProgress());

        if (Data.SettingsCustomAction.GetActionProgress() >= 1.0f) {
          endAction();
        }
      }

      if (IsState(MotorState.CustomAction)) {
        return;
      }

      if (IsState(MotorState.Jumping)) {
        if (m_Motor.Velocity.y < 0) {
          endJump();
        }
      }

      if (IsOnGround()) {
        bool dontChangeToOnGround =
            (IsState(MotorState.OnLadder) && !Data.SettingsOnLadder.ExitLadderOnGround) ||
            IsState(MotorState.Frozen);

        if (!dontChangeToOnGround) {
          changeState(MotorState.OnGround);
        }
      } else {
        if (IsState(MotorState.OnGround)) {
          m_FallingJumpPaddingFrame = calculateFramesFromTime(Data.SettingsJump.FallingJumpPaddingTime, timeStep);

          changeState(MotorState.Falling);
        }
      }

      if (IsState(MotorState.WallSliding)) {
        if (!IsAgainstWall()) {
          if (m_Motor.Velocity.y < 0.0f) {
            changeState(MotorState.Falling);
          } else {
            changeState(MotorState.Jumping);
          }
        }
      }
    }

    private void startJump (Vector2Int rawInput, int wallDirX) {
      bool success = false;

      if (IsState(MotorState.OnLedge)) {
        ledgeJump();
        success = true;
      } else if (IsState(MotorState.WallSliding)) {
        if (Data.SettingsWallInteraction.CanWallJump) {
          wallJump(rawInput.x, wallDirX);
          success = true;
        }
      } else {
        success = normalJump();
      }

      if (success) {
        consumeJumpPressed();

        changeState(MotorState.Jumping);

        OnJump.Invoke();
      }
    }

    private bool normalJump () {
      if (IsState(MotorState.OnGround)) {
        m_Velocity.y = Data.MaxJumpSpeed;

        OnNormalJump.Invoke();

        return true;
      } else if (IsState(MotorState.OnLadder)) {
        m_Velocity.y = Data.SettingsJump.OnLadderJumpForce;

        OnLadderJump.Invoke();

        return true;
      } else if (m_FallingJumpPaddingFrame >= 0) {
        m_Velocity.y = Data.MaxJumpSpeed;

        OnNormalJump.Invoke();

        return true;
      } else if (CanAirJump()) {
        m_Velocity.y = Data.MaxJumpSpeed;
        m_AirJumpCounter++;

        OnAirJump.Invoke();

        return true;
      } else {
        return false;
      }
    }

    private void ledgeJump () {
      m_Velocity.y = Data.MaxJumpSpeed;

      OnLedgeJump.Invoke();
    }

    private void wallJump (int rawInputX, int wallDirX) {
      bool climbing = wallDirX == rawInputX;
      Vector2 jumpVec;


      // climbing
      if (climbing || rawInputX == 0) {
        jumpVec.x = -wallDirX * Data.SettingsWallInteraction.ClimbForce.x;
        jumpVec.y = Data.SettingsWallInteraction.ClimbForce.y;
      }
      // jump leap
      else {
        jumpVec.x = -wallDirX * Data.SettingsWallInteraction.LeapForce.x;
        jumpVec.y = Data.SettingsWallInteraction.LeapForce.y;
      }

      OnWallJump.Invoke(jumpVec);

      m_Velocity = jumpVec;
    }

    private void consumeJumpPressed () {
      m_InputBuffer.IsJumpPressed = false;
      m_FallingJumpPaddingFrame = -1;
      m_WillJumpPaddingFrame = -1;
    }

    // highest point reached, start falling
    private void endJump () {
      if (IsState(MotorState.Jumping)) {
        changeState(MotorState.Falling);
      }
    }

    private void startDash (int rawInputX, float timeStep) {
      if (DashModule == null) {
        return;
      }

      if (IsState(MotorState.Dashing)) {
        return;
      }

      if (DashModule.CanOnlyBeUsedOnGround) {
        if (!IsOnGround()) {
          return;
        }
      }

      int dashDir = (rawInputX != 0) ? rawInputX : FacingDirection;
      if (!DashModule.CanDashToSlidingWall) {
        if (IsState(MotorState.WallSliding)) {
          int wallDir = (m_Motor.Collisions.Right) ? 1 : -1;
          if (dashDir == wallDir) {
            //Debug.Log("Dash Disallowed");
            return;
          }
        }
      }

      if (DashModule.ChangeFacing) {
        FacingDirection = (int) Mathf.Sign(m_Velocity.x);
      }

      if (!IsOnGround() && DashModule.UseGravity)
        m_Velocity.y = 0;

      Data.SettingsDash.Start(dashDir, timeStep);

      OnDash.Invoke(dashDir);

      changeState(MotorState.Dashing);
    }

    private void endDash () {
      if (IsState(MotorState.Dashing)) {
        // smooth out or sudden stop
        float vecX = Data.SettingsDash.DashDir * Data.SettingsDash.GetDashSpeed();
        m_VelocityXSmoothing = vecX;
        m_Velocity.x = vecX;
        changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
      }
    }

    private void startAction (int rawInputX) {
      if (ActionModule == null) {
        return;
      }

      if (IsState(MotorState.CustomAction)) {
        return;
      }

      if (DashModule.CanOnlyBeUsedOnGround) {
        if (!IsOnGround()) {
          return;
        }
      }

      int actionDir = (rawInputX != 0) ? rawInputX : FacingDirection;

      if (!ActionModule.CanUseToSlidingWall) {
        if (IsState(MotorState.WallSliding)) {
          int wallDir = (m_Motor.Collisions.Right) ? 1 : -1;
          if (actionDir == wallDir) {
            return;
          }
        }
      }

      Data.SettingsCustomAction.Start(actionDir);

      changeState(MotorState.CustomAction);
    }

    private void endAction () {
      if (IsState(MotorState.CustomAction)) {
        // smooth out or sudden stop
        Vector2 vec = new Vector2(Data.SettingsCustomAction.ActionDir, 1) * Data.SettingsCustomAction.GetActionVelocity();
        m_VelocityXSmoothing = vec.x;
        m_Velocity = vec;
        changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
      }
    }

    private void enterLadderState () {
      m_Velocity.x = 0;
      m_Velocity.y = 0;

      changeState(MotorState.OnLadder);

      m_AirJumpCounter = 0;
      OnResetJumpCounter.Invoke(MotorState.OnLadder);
      m_ApplyGravity = false;
    }

    private void exitLadderState () {
      m_Velocity.y = 0;
      changeState(IsOnGround() ? MotorState.OnGround : MotorState.Falling);
    }

    // change state and callback
    private void changeState (MotorState state) {
      if (m_MotorState == state) {
        return;
      }

      if (IsState(MotorState.OnLadder)) {
        m_ApplyGravity = true;
      }

      // exit old state action
      if (IsState(MotorState.Jumping)) {
        OnJumpEnd.Invoke();
      }

      if (IsState(MotorState.Dashing)) {
        OnDashEnd.Invoke();
      }

      if (IsState(MotorState.WallSliding)) {
        OnWallSlidingEnd.Invoke();
      }

      if (IsState(MotorState.OnLedge)) {
        OnLedgeGrabbingEnd.Invoke();
      }

      // set new state
      var prevState = m_MotorState;
      m_MotorState = state;

      if (IsState(MotorState.OnGround)) {
        m_AirJumpCounter = 0;
        OnResetJumpCounter.Invoke(MotorState.OnGround);

        if (prevState != MotorState.Frozen) {
          OnLanded.Invoke();
        }
      }

      if (IsState(MotorState.OnLedge)) {
        OnLedgeGrabbing.Invoke(Data.SettingsWallInteraction.WallDirX);
      }

      if (IsState(MotorState.WallSliding)) {
        OnWallSliding.Invoke(Data.SettingsWallInteraction.WallDirX);
      }

      OnMotorStateChanged.Invoke(prevState, m_MotorState);
    }

    private int calculateFramesFromTime (float time, float timeStep) {
      return Mathf.RoundToInt(time / timeStep);
    }

    private void onMotorCollisionEnter2D (MotorCollision2D col) {
      if (col.IsSurface(MotorCollision2D.CollisionSurface.Ground)) {
        onCollisionEnterGround();
      }

      if (col.IsSurface(MotorCollision2D.CollisionSurface.Ceiling)) {
        onCollisionEnterCeiling();
      }

      if (col.IsSurface(MotorCollision2D.CollisionSurface.Left)) {
        onCollisionEnterLeft();
      }

      if (col.IsSurface(MotorCollision2D.CollisionSurface.Right)) {
        onCollisionEnterRight();
      }
    }

    private void onCollisionEnterGround () {
      //Debug.Log("Ground!");
    }

    private void onCollisionEnterCeiling () {
      //Debug.Log("Ceiliing!");
    }

    private void onCollisionEnterLeft () {
      //Debug.Log("Left!");
    }

    private void onCollisionEnterRight () {
      //Debug.Log("Right!");
    }

    private void onMotorCollisionStay2D (MotorCollision2D col) {
      if (col.IsSurface(MotorCollision2D.CollisionSurface.Ground)) {
        onCollisionStayGround();
      }

      if (col.IsSurface(MotorCollision2D.CollisionSurface.Left)) {
        onCollisionStayLeft();
      }

      if (col.IsSurface(MotorCollision2D.CollisionSurface.Right)) {
        onCollisionStayRight();
      }
    }

    private void onCollisionStayGround () {
      //Debug.Log("Ground!");
      //m_AirJumpCounter = 0;
    }

    private void onCollisionStayLeft () {
      //Debug.Log("Left!");
      //if (m_WallJumpSettings.CanWallJump)
      //{
      //    m_AirJumpCounter = 0;
      //}
    }

    private void onCollisionStayRight () {
      //Debug.Log("Right!");
      //if (m_WallJumpSettings.CanWallJump)
      //{
      //    m_AirJumpCounter = 0;
      //}
    }
  }
}
