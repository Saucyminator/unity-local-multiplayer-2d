﻿using UnityEngine;
using Cinemachine;

namespace Saucy {
  public class CameraTargets : MonoBehaviour {
    [Header("References")] [SerializeField] private CinemachineVirtualCamera cam = null;
    [SerializeField] private CinemachineTargetGroup targetGroup = null;
    [SerializeField] private GameManager gameManager = null;
    [Header("Settings")] [SerializeField] private float CameraRadius = 2.5f;
    [SerializeField] private float CameraWeight = 1f;
    [Header("Listening on")] [SerializeField] private PlayerGameEvent eventPlayerJoined = null;
    [SerializeField] private PlayerGameEvent eventPlayerLeft = null;
    [SerializeField] private GameEvent eventGameStart = null;

    private void OnEnable () {
      eventPlayerJoined.OnEventRaised += PlayerJoined;
      eventPlayerLeft.OnEventRaised += PlayerLeft;
      eventGameStart.OnEventRaised += GameStart;
    }

    private void OnDisable () {
      eventPlayerJoined.OnEventRaised -= PlayerJoined;
      eventPlayerLeft.OnEventRaised -= PlayerLeft;
      eventGameStart.OnEventRaised -= GameStart;
    }

    private void PlayerJoined (Player _player) {
      targetGroup.AddMember(_player.gameObject.transform, CameraWeight, CameraRadius);
    }

    private void PlayerLeft (Player _player) {
      targetGroup.RemoveMember(_player.gameObject.transform);
    }

    private void GameStart () {
      for (int _index = 0; _index < gameManager.MaxPlayers; _index++) {
        if (!gameManager.Players.ContainsKey(_index)) {
          continue;
        }

        targetGroup.AddMember(gameManager.Players[_index].gameObject.transform, CameraWeight, CameraRadius);
      }

      cam.Follow = targetGroup.transform;
    }
  }
}
