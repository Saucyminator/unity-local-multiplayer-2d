using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// NOTE: There's no Editor script for Raise. Dunno how to do that.

namespace Saucy {
  [CreateAssetMenu(menuName = "Events/Player game event")]
  public class PlayerGameEvent : ScriptableObject {
    public event UnityAction<Player> OnEventRaised;
    [TextArea(3, 10)] public string devDescription = string.Empty;

    // The list of listeners that this event will notify if it is raised.
    private readonly List<PlayerGameEventListener> eventListeners = new List<PlayerGameEventListener>();

    public void Raise (Player _info) {
      OnEventRaised?.Invoke(_info);

      for (int i = eventListeners.Count - 1; i >= 0; i--) {
        eventListeners[i].OnEventRaised(_info);
      }
    }

    public void RegisterListener (PlayerGameEventListener _listener) {
      if (!eventListeners.Contains(_listener)) {
        eventListeners.Add(_listener);
      }
    }

    public void UnregisterListener (PlayerGameEventListener _listener) {
      if (eventListeners.Contains(_listener)) {
        eventListeners.Remove(_listener);
      }
    }
  }
}
