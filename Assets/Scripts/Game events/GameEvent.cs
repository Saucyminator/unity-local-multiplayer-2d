﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  [CreateAssetMenu(menuName = "Events/Game event")]
  public class GameEvent : ScriptableObject {
    public event UnityAction OnEventRaised;
    [TextArea(3, 10)] public string devDescription = string.Empty;

    // The list of listeners that this event will notify if it is raised.
    private readonly List<GameEventListener> eventListeners = new List<GameEventListener>();

    public void Raise () {
      OnEventRaised?.Invoke();

      for (int i = eventListeners.Count - 1; i >= 0; i--) {
        eventListeners[i].OnEventRaised();
      }
    }

    public void RegisterListener (GameEventListener _listener) {
      if (!eventListeners.Contains(_listener)) {
        eventListeners.Add(_listener);
      }
    }

    public void UnregisterListener (GameEventListener _listener) {
      if (eventListeners.Contains(_listener)) {
        eventListeners.Remove(_listener);
      }
    }
  }
}
