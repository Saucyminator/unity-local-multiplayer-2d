using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  public class PlayerGameEventListener : MonoBehaviour {
    public bool listenForMultipleEvents = false;
    [Tooltip("Useful for toggling things on and then off.")] public bool useDelayedResponse = false;
    public float delay = 0f;

    [Tooltip("Event to listen to.")] public PlayerGameEvent Event = null;
    [Tooltip("Events to listen.")] public List<PlayerGameEvent> Events = new List<PlayerGameEvent>();

    [Tooltip("Response when Event(s) are raised.")] public PlayerEvent Response;
    [Tooltip("A delayed response when Event(s) are raised.")] public PlayerEvent DelayedResponse;

    private void OnEnable () {
      if (!listenForMultipleEvents) {
        if (Event == null) {
          throw new System.NullReferenceException($"No PlayerGameEvent asset assigned to {this}.");
        } else {
          Event.RegisterListener(this);
        }
      } else {
        foreach (PlayerGameEvent _event in Events) {
          if (_event == null) {
            throw new System.NullReferenceException($"No PlayerGameEvent asset assigned to {this}.");
          } else {
            _event.RegisterListener(this);
          }
        }
      }
    }

    private void OnDisable () {
      if (!listenForMultipleEvents) {
        if (Event == null) {
          throw new System.NullReferenceException($"No PlayerGameEvent asset assigned to {this}.");
        } else {
          Event.UnregisterListener(this);
        }
      } else {
        foreach (PlayerGameEvent _event in Events) {
          if (_event == null) {
            throw new System.NullReferenceException($"No PlayerGameEvent asset assigned to {this}.");
          } else {
            _event.UnregisterListener(this);
          }
        }
      }
    }

    public void OnEventRaised (Player _info) {
      Response.Invoke(_info);

      if (useDelayedResponse && gameObject.activeSelf) {
        StartCoroutine(RunDelayedEvent(_info));
      }
    }

    private IEnumerator RunDelayedEvent (Player _info) {
      yield return new WaitForSeconds(delay);

      DelayedResponse?.Invoke(_info);
    }
  }
}
