﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  public class GameEventListener : MonoBehaviour {
    public bool listenForMultipleEvents = false;
    [Tooltip("Useful for toggling things on and then off.")] public bool useDelayedResponse = false;
    public float delay = 0f;

    [Tooltip("Event to listen to.")] public GameEvent Event = null;
    [Tooltip("Events to listen.")] public List<GameEvent> Events = new List<GameEvent>();

    [Tooltip("Response when Event(s) are raised.")] public UnityEvent Response;
    [Tooltip("A delayed response when Event(s) are raised.")] public UnityEvent DelayedResponse;

    private void OnEnable () {
      if (!listenForMultipleEvents) {
        if (Event == null) {
          throw new System.NullReferenceException($"No GameEvent asset assigned to {this}.");
        } else {
          Event.RegisterListener(this);
        }
      } else {
        foreach (GameEvent _event in Events) {
          if (_event == null) {
            throw new System.NullReferenceException($"No GameEvent asset assigned to {this}.");
          } else {
            _event.RegisterListener(this);
          }
        }
      }
    }

    private void OnDisable () {
      if (!listenForMultipleEvents) {
        if (Event == null) {
          throw new System.NullReferenceException($"No GameEvent asset assigned to {this}.");
        } else {
          Event.UnregisterListener(this);
        }
      } else {
        foreach (GameEvent _event in Events) {
          if (_event == null) {
            throw new System.NullReferenceException($"No GameEvent asset assigned to {this}.");
          } else {
            _event.UnregisterListener(this);
          }
        }
      }
    }

    public void OnEventRaised () {
      Response.Invoke();

      if (useDelayedResponse && gameObject.activeSelf) {
        StartCoroutine(RunDelayedEvent());
      }
    }

    private IEnumerator RunDelayedEvent () {
      yield return new WaitForSeconds(delay);

      DelayedResponse?.Invoke();
    }
  }
}
