﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

// NOTE: Keeps track of Avatar, PlayerName, HP, etc.

namespace Saucy {
  public class UIPlayer : MonoBehaviour {
    [SerializeField] private Image icon = null;
    [SerializeField] private Image frame = null;
    [Header("Listening on")] [SerializeField] private PlayerGameEvent eventPlayerDataUpdated = null;

    private Player player;

    private void OnEnable () {
      eventPlayerDataUpdated.OnEventRaised += PlayerDataUpdated;
    }

    private void OnDisable () {
      eventPlayerDataUpdated.OnEventRaised -= PlayerDataUpdated;
    }

    public void SetPlayer (Player _player) {
      player = _player;

      UpdateFrame(player.Color.Color);
      UpdateIcon(player.Data.Avatar);
    }

    private void PlayerDataUpdated (Player _player) {
      if (player != _player) {
        return;
      }

      UpdateFrame(player.Color.Color);
      UpdateIcon(player.Data.Avatar);
    }

    public void UpdateFrame (Color _color) => frame.color = _color;

    public void UpdateIcon (Sprite _icon) => icon.sprite = _icon;
  }
}
