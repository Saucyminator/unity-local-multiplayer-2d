﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// TODO: Make this modular, remove references to UIMenu because it's not needed I believe.
// TODO: Should only update the UI, not actually update the graphics on the player!

namespace Saucy {
  public class UIMenuPlayerCharacter : MonoBehaviour {
    [Header("References")] [SerializeField] private TextMeshProUGUI playerSlot = null;
    [SerializeField] private TextMeshProUGUI characterName = null;
    [SerializeField] private GameObject joinText = null;
    [SerializeField] private Image image = null;
    [SerializeField] private GameObject confirm = null;
    [SerializeField] private GameObject ready = null;
    [Header("Settings")] [Min(0)] [SerializeField] private int playerIndex = 0;
    [Header("Listening on")] [SerializeField] private PlayerGameEvent eventPlayerJoined = null;
    [SerializeField] private PlayerGameEvent eventPlayerLeft = null;
    [SerializeField] private PlayerGameEvent eventPlayerReady = null;
    [SerializeField] private PlayerGameEvent eventPlayerUnready = null;
    [SerializeField] private PlayerGameEvent eventPlayerDataUpdated = null;

    private Player player;
    private DataColor currentColor = null;
    private DataCharacter currentCharacter = null;

    private void Awake () {
      playerSlot.text = $"Player {playerIndex + 1}";
      playerSlot.gameObject.SetActive(true);
    }

    private void OnEnable () {
      eventPlayerJoined.OnEventRaised += PlayerJoined;
      eventPlayerLeft.OnEventRaised += PlayerLeft;
      eventPlayerReady.OnEventRaised += PlayerReady;
      eventPlayerUnready.OnEventRaised += PlayerUnready;
      eventPlayerDataUpdated.OnEventRaised += UpdateUI;
    }

    private void OnDisable () {
      eventPlayerJoined.OnEventRaised -= PlayerJoined;
      eventPlayerLeft.OnEventRaised -= PlayerLeft;
      eventPlayerReady.OnEventRaised -= PlayerReady;
      eventPlayerUnready.OnEventRaised -= PlayerUnready;
      eventPlayerDataUpdated.OnEventRaised -= UpdateUI;
    }

    public void PlayerJoined (Player _player) {
      if (_player.PlayerIndex != playerIndex) {
        return;
      }

      player = _player;

      characterName.gameObject.SetActive(true);
      joinText.SetActive(false);
      image.gameObject.SetActive(true);
      confirm.SetActive(true);
      ready.SetActive(false);

      UpdateUI(player);
    }

    public void PlayerLeft (Player _player) {
      if (_player.PlayerIndex != playerIndex) {
        return;
      }

      characterName.gameObject.SetActive(false);
      joinText.SetActive(true);
      image.gameObject.SetActive(false);
      confirm.SetActive(false);
      ready.SetActive(false);

    }

    public void PlayerReady (Player _player) {
      if (_player.PlayerIndex != playerIndex) {
        return;
      }

      confirm.SetActive(false);
      ready.SetActive(true);
    }

    public void PlayerUnready (Player _player) {
      if (_player.PlayerIndex != playerIndex) {
        return;
      }

      confirm.SetActive(true);
      ready.SetActive(false);
    }

    private void UpdateUI (Player _player) {
      if (_player != player) {
        return;
      }

      currentColor = _player.Color;
      currentCharacter = _player.Data;

      characterName.text = currentCharacter.Name;
      image.color = currentColor.Color;
      image.sprite = currentCharacter.Avatar;
    }
  }
}
