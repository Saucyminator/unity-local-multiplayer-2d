﻿using UnityEngine;
using UnityEngine.Events;

namespace Saucy {
  public class UIBack : MonoBehaviour {
    [SerializeField] private GameManager gameManager = null;
    public UnityEvent OnBack;

    private void OnEnable () {
      gameManager.OnBack.AddListener(Back);
    }

    private void OnDisable () {
      gameManager.OnBack.RemoveListener(Back);
    }

    private void Back () {
      OnBack?.Invoke();
    }
  }
}
