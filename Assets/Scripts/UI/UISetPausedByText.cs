﻿using UnityEngine;
using TMPro;

namespace Saucy {
  public class UISetPausedByText : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI label = null;
    [SerializeField] private string format = "Paused by Player #{0}";

    public void SetText (string _text) {
      label.text = string.Format(format, _text);
    }

    public void SetText (Player _player) {
      SetText(_player.PlayerSlot.ToString());
    }
  }
}
