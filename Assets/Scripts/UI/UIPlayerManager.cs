﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: Only keeps track of adding/removing player for when the game starts.

namespace Saucy {
  public class UIPlayerManager : MonoBehaviour {
    [SerializeField] private GameManager gameManager = null;
    [SerializeField] private GameObject playerUIPrefab = null;
    [SerializeField] private Transform container = null;

    [Header("Listening on")] [SerializeField] private PlayerGameEvent eventPlayerJoined = null;
    [SerializeField] private PlayerGameEvent eventPlayerLeft = null;
    [SerializeField] private GameEvent eventGameStart = null;

    private Dictionary<int, UIPlayer> playerUIList = new Dictionary<int, UIPlayer>();

    private void OnEnable () {
      eventPlayerJoined.OnEventRaised += PlayerJoined;
      eventPlayerLeft.OnEventRaised += PlayerLeft;
      eventGameStart.OnEventRaised += GameStart;
    }

    private void OnDisable () {
      eventPlayerJoined.OnEventRaised -= PlayerJoined;
      eventPlayerLeft.OnEventRaised -= PlayerLeft;
      eventGameStart.OnEventRaised -= GameStart;
    }

    private void Start () {
      if (gameManager.Players.Count > 0) {
        StartCoroutine(BuildUI());
      }
    }

    private void GameStart () {
      Debug.Log($"UIPlayerManager: GameStart");
      StartCoroutine(BuildUI());
    }

    private void PlayerJoined (Player _player) {
      Debug.Log($"UIPlayerManager: Player #{_player.PlayerSlot} joined, building UI");

      if (!playerUIList.ContainsKey(_player.PlayerIndex)) {
        UIPlayer _ui = (UIPlayer) Instantiate(playerUIPrefab, container.position, container.rotation, container).GetComponent<UIPlayer>();
        _ui.SetPlayer(_player);
        _ui.gameObject.name = $"Player {_player.PlayerSlot} UI";

        playerUIList.Add(_player.PlayerIndex, _ui);
      }
    }

    private void PlayerLeft (Player _player) {
      Debug.Log($"UIPlayerManager: Player #{_player.PlayerSlot} left, removing UI");

      if (playerUIList.ContainsKey(_player.PlayerIndex)) {
        Destroy(playerUIList[_player.PlayerIndex].gameObject);
        playerUIList.Remove(_player.PlayerIndex);
      }
    }

    private IEnumerator BuildUI () {
      Debug.Log($"Building UI...");

      for (int _index = 0; _index < gameManager.MaxPlayers; _index++) {
        if (!gameManager.Players.ContainsKey(_index) || playerUIList.ContainsKey(_index)) {
          continue;
        }

        // yield return new WaitUntil(() => gameManager.Players[_index].GetComponent<PlayerHealth>().CurrentHealth > 0);
        yield return new WaitForSeconds(1f);

        UIPlayer _ui = (UIPlayer) Instantiate(playerUIPrefab, container.position, container.rotation, container).GetComponent<UIPlayer>();
        _ui.SetPlayer(gameManager.Players[_index]);
        _ui.gameObject.name = $"Player {_index + 1} UI";

        if (!playerUIList.ContainsKey(_index)) {
          playerUIList.Add(_index, _ui);
        }
      }
    }
  }
}
