using UnityEngine;

namespace Saucy {
  public class UISounds : MonoBehaviour {
    [SerializeField] private AudioClip[] clips = null;
    [SerializeField] private AudioSource source = null;

    public void PlaySound (float _index) {
      PlaySound((int) _index);
    }

    public void PlaySound (int _index) {
      if (clips[_index] == null) {
        Debug.LogWarning($"No clip at index: {_index}");
        return;
      }

      source.PlayOneShot(clips[_index]);
    }
  }
}
