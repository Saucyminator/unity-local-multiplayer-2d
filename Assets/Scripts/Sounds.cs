﻿using UnityEngine;

namespace Saucy {
  public class Sounds : MonoBehaviour {
    [SerializeField] private bool useSounds = true;
    [SerializeField] private AudioSource soundsSource = null;

    public void StopAllSounds () => soundsSource.Stop();

    public void PlaySound (AudioClip _clip) {
      if (useSounds) {
        soundsSource.PlayOneShot(_clip);
      }
    }
  }
}
