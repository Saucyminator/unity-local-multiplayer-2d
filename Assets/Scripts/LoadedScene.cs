﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Saucy {
  public class LoadedScene : MonoBehaviour {
    [Tooltip("Skip calling the GameStart event if the loaded scene has this build index (in this project it's the menu).")] [SerializeField] private int skipCallingEventOnBuildIndex = 0;

    public UnityEvent GameStart;

    private void OnEnable () {
      SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable () {
      SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded (Scene _scene, LoadSceneMode _mode) {
      if (_scene.buildIndex != skipCallingEventOnBuildIndex) {
        GameStart?.Invoke();
      }
    }
  }
}
